<?php

$lang['required']			= "%s 不能为空.";
$lang['isset']				= "%s 不能为空.";
$lang['valid_email']		= "%s 必须为有效的邮箱地址.";
$lang['valid_emails']		= "%s 必须为有效的邮箱地址.";
$lang['valid_url']			= "%s 必须包含有效的URL地址.";
$lang['valid_ip']			= "%s 必须包含有效的IP地址.";
$lang['min_length']			= "%s 长度不能少于  %s 字符.";
$lang['max_length']			= "%s 长度不能超过  %s 字符.";
$lang['exact_length']		= "%s 长度必须为  %s 字符.";
$lang['alpha']				= "%s 只能包含英文字母.";
$lang['alpha_numeric']		= "%s 只能包含英文字符或者数字 .";
$lang['alpha_dash']			= "%s 只能包含英文字母,下划线,破折号.";
$lang['numeric']			= "%s 只能包含数字.";
$lang['is_numeric']			= "%s 只能包含数字字符.";
$lang['integer']			= "%s 只能包含整数.";
$lang['regex_match']		= "%s 不是一个正确的格式";
$lang['matches']			= "未匹配到  %s.";
$lang['is_unique'] 			= "%s 必须包含唯一值.";
$lang['is_natural']			= "%s 必须包含正数.";
$lang['is_natural_no_zero']	= "%s 必须包含一个大于零的数.";
$lang['decimal']			= "%s 必须包含一个十进制数.";
$lang['less_than']			= "必须包含一个小于 %s 的值.";
$lang['greater_than']		= "%s 必须包含一个大于  %s 的值.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */