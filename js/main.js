/**
 * @title 全局通用
 * @author fangyang
 * @since 20130406
 */

function SelectAll() {
	var checkboxs = document.getElementsByName("checkboxid");
	for ( var i = 0; i < checkboxs.length; i++) {
		var e = checkboxs[i];
		e.checked = !e.checked;
	}
}

function getCheckboxItem() {
	var allSel = "";
	if (document.form2.arcID.value)
		return document.form2.arcID.value;
	for (i = 0; i < document.form2.arcID.length; i++) {
		if (document.form2.arcID[i].checked) {
			if (allSel == "")
				allSel = document.form2.arcID[i].value;
			else
				allSel = allSel + "`" + document.form2.arcID[i].value;
		}
	}
	return allSel;
}

// 滚动后固定 DIV
$(function() {
	var obj = $("#toolbar");
	var offset = obj.offset();
	if(offset==null)return;
	var bktop = offset.top;
	var bkleft = offset.left;
	var bkright = offset.right;
	var bkwidth = obj.width();
	var bs_isIE = !!window.ActiveXObject;
	var bs_isIE6 = bs_isIE && !window.XMLHttpRequest;
	var bodywidth = $(document.body).width();
	var bodyheight = $(document.body).height();
	$(window).scroll(function() {
		// $("#kk").html(bodyheight);
		var weitt = $(window).scrollTop();
		if (weitt > (bktop - 40)) {
			$("#toolbar").css({
				"position" : "fixed",
				"top" : "43px",
				"background" : "#fff",
				"width" : bkwidth,
				"left" : bkleft,
				"right" : bkleft,
			});
			if (bs_isIE) {// IE6
				if (bs_isIE6) {
					$("#toolbar").css({
						"position" : "absolute",
						"top" : weitt,
						"left" : bkleft
					});
				}
			}
		} else {
			$("#toolbar").css({
				"position" : "static"
			});
		}
	})
})
