<?php
if(!defined('BASEPATH'))
{
    exit ('No direct script access allowed');
}
set_time_limit(0);

/**
 * @title
 *
 * @author fangyang (funyung@163.com)
 * @since  2013-4-5 6:24:28
 *
 */
class Urlzhanwei extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper(array(
            'form',
            'url'
        ));
        $this->load->library('form_validation');
        $this->load->library('security');
        $this->load->library('fx_auth');
        $this->load->library('table');
        $this->load->model('zhanweikeyword');
        $this->load->model('ZhanweiURL');
        $this->load->model('Rank_baidu_model');

        if(!$this->fx_auth->is_logged_in())
        {
            redirect('/auth/send_again/');
        }

    }

    /**
     * index
     */
    public function index()
    {
        static $data = array(),$startdate = '',$endate = '',$status = '';
        if($_GET)
        {
            $po = &$_GET;
            if($po['action'] == 'sall')
            {
                //时间计算
                if(isset ($po ['startdate']) && isset ($po ['endate']))
                {
                    $startdate = $po ['startdate'];
                    $endate    = $po ['endate'];
                } else if(!isset ($po ['startdate']) && isset ($po ['endate']))
                {
                    $endate    = $po ['endate'];
                    $startdate = $endate;

                } else if(isset ($po ['startdate']) && !isset ($po ['endate']))
                {
                    $startdate = $po ['startdate'];
                    $endate    = $startdate;
                }

                //需要筛选的字段
                $filterArr = array(
                    'uid'           => $po['uid'],
                    'searchdate >=' => $startdate,
                    'searchdate <=' => $endate,
                );

                //当时间都为空
                if(empty($po ['startdate']) && empty ($po ['endate']))
                {
                    $filterArr = array(
                        'uid' => $po['uid'],
                    );
                }
                //用户
                if(!empty($po['name']))
                {
                    $filterArr['author'] = $po['name'];

                }

                //占位
                switch (@$po['status'])
                {
                    case 'have' :
                        $filterArr['rank !='] = '';
                        $status               = 'have';
                        break;
                    case 'none':
                        $filterArr['rank ='] = '';
                        $status              = 'none';
                        break;
                    default:
                        $status = 'all';
                }

                $where             = $filterArr;
                $data['startdate'] = $startdate;
                $data['endate']    = $endate;
                $data['status']    = $status;
                $data['unit']      = $po['uid'];
                $data['author']    = isset($po['name']) ? $po['name'] : '';
                $unit              = $po['uid'];

            }
        } else
        {

            //$unit = $this->uri->uri_to_assoc(3);
            $unit           = '1';
            $where          = array(
                'uid'    => isset($unit['unit']) ? $unit['unit'] : '1',
                'author' => $this->fx_auth->get_username()
            );
            $data['status'] = 'all';
            $data['unit']   = isset($unit['unit']) ? $unit['unit'] : '1';
        }

        //分页参数传递
        $startdate = isset ($po ['startdate']) ? $po ['startdate'] : '';
        $endate    = isset ($po ['endate']) ? $po ['endate'] : '';
        $name      = isset ($po ['name']) ? $po ['name'] : '';

        //$uri = $this->uri->segment ( 12 ); //分页所在段
        //$offset = (! empty ( $uri ) && is_numeric ( $uri )) ? $uri : 0;
        $offset                = (!empty ($_GET['per_page']) && is_numeric($_GET['per_page'])) ? $_GET['per_page'] : 0;
        $per_page              = 5000;
        $keyword_data          = $this->zhanweikeyword->default_keyword_rank($per_page,$offset,$where);
        $total                 = count($this->zhanweikeyword->fetch_all($where));
        $data['total']         = $total;
        $data ['keyword_data'] = $keyword_data;

        /*
         * 分页开始
         */
        //
        //        $this->load->library('pagination');
        //        $config                      = array();
        //        $config['page_query_string'] = true;
        //        //$config['enable_query_strings'] =TRUE;
        //        //$config ['uri_segment'] = 6;
        //        //$config ['base_url'] = site_url ( 'rank_keyword/index/unit/' . $data['unit'].'/startdate/'.$startdate.'/endate/'.$endate.'/status/'.$status );
        //        $config ['base_url']         = site_url('rank_keyword/index/?action=sall&status='.$status.'&name='.$name.'&uid='.$unit.'&startdate='.$startdate.'&endate='.$endate);
        //        $config ['total_rows']       = $total;
        //        $config ['per_page']         = $per_page;
        //        $config ['full_tag_open']    = '<div class="pagination pagination-small pagination-right"><ul>';
        //        $config ['full_tag_close']   = '</ul></div>';
        //        $config ['first_link']       = '首页';
        //        $config ['last_link']        = '末页';
        //        $config ['first_tag_open']   = '<li>';
        //        $config ['first_tag_close']  = '</li>';
        //        $config ['prev_link']        = '«';
        //        $config ['prev_tag_open']    = '<li class="prev">';
        //        $config ['prev_tag_close']   = '</li>';
        //        $config ['next_link']        = '»';
        //        $config ['next_tag_open']    = '<li>';
        //        $config ['next_tag_close']   = '</li>';
        //        $config ['last_tag_open']    = '<li>';
        //        $config ['last_tag_close']   = '</li>';
        //        $config ['cur_tag_open']     = '<li class="active"><a href="#">';
        //        $config ['cur_tag_close']    = '</a></li>';
        //        $config ['num_tag_open']     = '<li>';
        //        $config ['num_tag_close']    = '</li>';
        //        $config ['num_links']        = 1;
        //        $config ['use_page_numbers'] = false;
        //
        //        $this->pagination->initialize($config);

        $data ['hospital_unit'] = $this->zhanweikeyword->getHospitalunit();
        $data['users']          = $this->users->get_all()->result();
        //已经查询过的关键词
        $data['searched'] = $this->zhanweikeyword->getSearched(@$po['name'],@$po['uid']);
        // 载入视图
        $this->load->view('urlzhanwei/index',$data);

    }

    /**
     * insert keywords page
     * @return null
     */
    public function insertView()
    {
        $data ['hospital_unit'] = $this->zhanweikeyword->getHospitalunit();
        $this->load->view('urlzhanwei/insert',$data);
    }

    /**
     * insert keywords data from index
     */
    public function insert()
    {
        if(isset ($_POST ['uid']) && isset ($_POST ['keyword']) && isset ($_POST ['author']))
        {
            $keyword = $_POST ['keyword'];
            $edata   = explode("\n",$keyword);
            $edata   = $this->zhanweikeyword->delArrayRepeat($edata);

            $is_repeat = isset($_POST['is_repeat']) ? $_POST['is_repeat'] : '';
            foreach ($edata as $a => $b)
            {
                $data = array(
                    'uid'        => $_POST ['unit'],
                    'keyword'    => trim($b),
                    'insertdate' => date('Y-m-d H:s:m'),
                    'author'     => $this->fx_auth->get_username()
                );
                //禁止重复   //重复分两种   当前账户重复  与其他账户重复
                if($this->zhanweikeyword->is_exist(trim($b),$is_repeat))
                {
                    $this->zhanweikeyword->insert_keyword($data);
                }
            }

            //url
            $keyword = $_POST ['url'];
            $edata   = explode("\n",$keyword);
            $edata   = $this->zhanweikeyword->delArrayRepeat($edata);

            $this->ZhanweiURL->deleteURL('admin');

            foreach ($edata as $a => $b)
            {
                $data = array(
                    'uid'        => $_POST ['unit'],
                    'url'        => trim($b),
                    'insertdate' => date('Y-m-d H:s:m'),
                    'author'     => $this->fx_auth->get_username()
                );
                //禁止重复   //重复分两种   当前账户重复  与其他账户重复
                if($this->ZhanweiURL->is_exist(trim($b),$is_repeat))
                {
                    $this->ZhanweiURL->insert_URL($data);
                }
            }

            redirect('/urlzhanwei/');
        } else
        {
            $this->insertView();
        }

    }

    /**
     * delete select data
     */
    public function delete()
    {

        if($this->zhanweikeyword->delete_keyword($this->fx_auth->get_username()))
        {
            echo 'success';
        } else
        {
            echo 'failure';
        }
    }

    /**
     * 百度查询结果
     */
    function baiduRank()
    {
        $unit    = $_POST ['unit'];
        $id      = $_POST ['id'];
        $keyword = $this->zhanweikeyword->getKeyword($id);
        $unit    = $this->zhanweikeyword->getUnit($unit);
        $this->search($keyword,$unit,$id);
        $resultArr            = $this->zhanweikeyword->rankResult($id,'zhanwei_keyword');
        $backValue            = array();
        $backValue['status']  = 'success';
        $backValue['id']      = $id;
        $backValue['rank']    = $this->indexZhan($resultArr[0]['rank']);
        $backValue['compete'] = $resultArr[0]['compete'];
        // $backValue['snapshot']='无快照';//$resultArr[0]['snapshot'];
        $backValue['num_spread'] = $resultArr[0]['num_spread'];
        $backValue['searchdate'] = $resultArr[0]['searchdate'];
        echo json_encode($backValue);

    }

    /**
     * 推广格式
     *
     * @param string $data
     *
     * @return string
     */
    function tuiguang($data = '')
    {
        static $num_spread,$result;
        $tbaidu     = array(
            '上',
            '下',
            '右'
        );
        $num_spread = $data;
        if(!isset($num_spread))
        {
            $data = '无';
        } else
        {
            $num_spread = explode(',',$num_spread);
            foreach ($num_spread as $v => $n)
            {
                $result .= $tbaidu [$v].':<b style="color:#b94a48">'.$n.'</b>&nbsp;';
            }
        }

        return $result;
    }

    /**
     * 首页占位格式
     *
     * @param string $data
     *
     * @return string
     */
    function indexZhan($data)
    {
        $rank = $data;
        if(empty($rank))
        {
            return '无';
        } else
        {
            //return $rank.'&nbsp;|&nbsp;<b style="color:#b94a48">'.count(explode(',',$rank)).'</b>';
            return $rank;
        }
    }

    /**
     * changesUnit
     */
    public function changeUnit()
    {
        $unit = $_GET ['unit'];

        $keyword_data = $this->zhanweikeyword->default_keyword_rank('','');
        echo 'success';

    }

    /**
     * to excel .
     * csv
     */
    public function toCsv()
    {
        static $table = 'keyword_rank';
        $this->load->dbutil();
        $query = $this->db->query("SELECT * FROM $table where uid='1'");
        echo $this->dbutil->csv_from_result($query);
    }

    /**
     * 查询方法
     *
     * @param     $keyword
     * @param     $unit
     * @param     $id
     * @param int $page
     */
    public function search()
    {

        $unit     = $this->input->get('u',true);
        $keyword  = (array)base64_decode($this->input->get('k',true));
        $id       = $this->input->get('id',true);
        $callback = $this->input->get('callback',true);


        $idArray = explode('|',$id);
        //$id = $idArray;
        foreach ($keyword as $v => $keyword)
        {

            static $px = 0,$zw = 0,$ezw = 0;
            $urlArray  = array();
            $keyArray  = array();
            $rankArray = array();
            $id        = $idArray[$v];
            $rsState   = false;
            $enKeyword = urlencode($keyword);
            //$firstRow  = ($page - 1) * 10;
            $urls      = $this->ZhanweiURL->fetch_all();

            $snoopy = new Snoopy ();
            $snoopy->fetch("http://www.baidu.com/s?wd=$enKeyword");

            $contents = $snoopy->results;

            // 推广位
            $ebaidu = array(
                'lt' => 0,
                'lb' => 0,
                'r'  => 0
            );

            $num_spread = $ebaidu ['lt'].','.$ebaidu ['lb'].','.$ebaidu ['r'];

            //preg_match_all ( '/<table[^>]*?class="result"[^>]*>[\s\S]*?<\/table>/i', $contents, $rs ); // 首页占位
            //新规则
            preg_match_all('/<div[^>]*?class="result c-container "[^>]*>[\s\S]*?<\/div>/i',$contents,$rs); // 首页占位

            preg_match_all('/<div class="f13"><span class="g">[\s\S]*?<\/span>/',$contents,$rs_t);

            $rankK      = $position = 0;
            $num_spread = [];
            foreach ($rs_t [0] as $k => $v)
            {
                $position++;
                foreach ($urls as $uv)
                {
                    if(strstr($v,$uv->url))
                    {
                        $rankK++;
                        $num_spread[] = $position;
                    }
                }
            }

            /*
            $mh = curl_multi_init ();
            foreach ( $urlArray as $i => $url )
            {
                $conn [$i] = curl_init ( $url );
                curl_setopt ( $conn [$i], CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)" );
                curl_setopt ( $conn [$i], CURLOPT_HEADER, 0 );
                curl_setopt ( $conn [$i], CURLOPT_CONNECTTIMEOUT, 10 );
                curl_setopt ( $conn [$i], CURLOPT_RETURNTRANSFER, true ); // 设置不将爬取代码写到浏览器，而是转化为字符串
                curl_setopt ( $conn [$i], CURLOPT_FOLLOWLOCATION, true ); // 使用自动跳转
                curl_multi_add_handle ( $mh, $conn [$i] );
            }
            do
            {
                curl_multi_exec ( $mh, $active );
            } while ( $active );

            unset($zw); //多次循环、莫名递增！！

            foreach ( $urlArray as $i => $url )
            {
                @$zw ++;
                $data = $this->my_encoding ( curl_multi_getcontent ( $conn [$i] ), 'utf-8' ); // ("UTF-8","GBK//IGNORE",curl_multi_getcontent($conn[4]));
                if (preg_match ( "/.$unit./", $data ))
                {
                    $rankArray [] = $zw;
                }
            }
            */
            // 抓取所有链接
            /*
             * $snoopy->fetchlinks("http://www.baidu.com/s?wd=$enKeyword");
             * $contentlink=$snoopy->results; $a; foreach ($contentlink as
             * $r=>$url) { if(strstr('http://www.baidu.com/link?url',$url)) {
             * $a++; //unset($contentlink[$r]); //print_r($contentlink); } }
             */

            // 写入
            $resultArray = array(
                'snapshot'   => '',
                'rank'       => $rankK,
                //'compete' => $this->key_weight ( $num_spread ),
                'num_spread' => implode(',',$num_spread),
                'searchdate' => date('Y-m-d H:s:m')
            );

            //保存结果
            $this->saveResult($id,$resultArray,'zhanwei_keyword');

            //移除
            /*
            foreach ($urlArray as $i => $url) {

                curl_multi_remove_handle($mh,$conn[$i]);
                curl_close($conn[$i]);
            }

            curl_multi_close ( $mh );
            */
            $resultArray['state'] = 1;
            echo $callback.'('.json_encode($resultArray).')';

            unset ($contents,$zw,$v,$rs_t,$data,$snoopy,$id,$rarray,$urlArray,$rankArray,$resultArray,$i);



            // 多页查询
            /*
             * if ($rsState === false) { $this->search($keyword, $url,$id, ++
             * $page); }
             */
        }
    }

    /**
     * 编码转换
     *
     * @param $data
     * @param $to
     *
     * @return string
     */
    public function my_encoding($data,$to)
    {
        $encode_arr = array(
            'UTF-8',
            'ASCII',
            'GBK',
            'GB2312',
            'BIG5',
            'JIS',
            'eucjp-win',
            'sjis-win',
            'EUC-JP'
        );
        $encoded    = mb_detect_encoding($data,$encode_arr);
        $data       = mb_convert_encoding($data,$to,$encoded);

        return $data;
    }

    /**
     * 保存查询结果
     *
     * @param        $id
     * @param array  $resultArray
     * @param string $db
     *
     * @return void
     */
    public function saveResult($id,$resultArray,$db)
    {

        $this->db->where('id',$id);
        $this->db->update($db,$resultArray);
        //return $id;
    }
}
 