<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         订餐//其他功能
 * @author        fangyang (funyung@163.com)
 * @since         2013-5-1 10:20
 *     
 */

class Order extends CI_Controller
{
	public function __construct ()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('security');
		$this->load->library('fx_auth');
		$this->load->library('table');
		$this->load->model('Company_model');
		$this->load->model('Order_model');
		
	    if(!$this->fx_auth->is_logged_in())
	    {
	    	
	    	if($_SERVER["REMOTE_ADDR"] != '106.120.97.50')
	    	{
	    		redirect('error/ip_not_allow');
	    		
	    	}else{
	    		
	    	}
	    }
	    
	}

	/**
	 *
	 */
	public function index()
	{
		$data = array();
        
		$data['users'] = $this->Company_model->fetch_all();
		
		$this->load->view('order/index',$data);
	}
	
	/**
	 * 插入界面
	 * @return null
	 */
	public function insertView()
	{
		$data ['hospital_unit'] = $this->keyword_rank_model->getHospitalunit ();
		$this->load->view ( 'rank_keyword/insert', $data );
	}
	
	public function insert()
	{
		if (isset ( $_POST ['uid'] ) && isset ( $_POST ['keyword'] ) && isset ( $_POST ['author'] ))
		{
			$keyword = $_POST ['keyword'];
			$edata = array ();
			$edata = explode ( "\n", $keyword );
			$edata = $this->keyword_rank_model->delArrayRepeat ( $edata );
			foreach ( $edata as $a => $b )
			{
				$data = array (
						'uid' => $_POST ['unit'],
						'keyword' => trim ( $b ),
						'insertdate' => date ( 'Y-m-d H:s:m' ),
						'author' => $this->fx_auth->get_username ()
				);
	
				$this->keyword_rank_model->insert_keyword ( $data );
			}
				
			redirect('/rank_keyword/');
		} else
		{
				
			$this->insertView ();
		}
	}
	
	/**
	 * 更新订餐记录
	 * 
	 * @param
	 * @return
	 */
    function orderChange()
	{
		$data = array();
		$today = date('Y-m-d');
		$tomorrow1 = date("Y-m-d 6:00:00", mktime(0,0,0,date("m"),date("d")+1,date("Y")));
		$tomorrow2 = date("Y-m-d 18:00:00", mktime(0,0,0,date("m"),date("d")+1,date("Y")));
		$acquired1 = date("Y-m-d 6:00:00", mktime(0,0,0,date("m"),date("d")+2,date("Y")));
		$acquired2 = date("Y-m-d 18:00:00", mktime(0,0,0,date("m"),date("d")+2,date("Y")));
		
		if(isset($_POST['when'])&&isset($_POST['is_order']))
		{
			
			$whenArr = explode('/',$_POST['when']);
			switch($whenArr[0])
			{
				case 'tomorrow1':
					$ordertime = $tomorrow1;
					break;
				case 'tomorrow2':
					$ordertime = $tomorrow2;
					break;
				case 'acquired1':
					$ordertime = $acquired1;
					break;
				case 'acquired2':
					$ordertime = $acquired2;
					break;
						
			}
			
			$data['ordertime'] = $ordertime;
			$data['uid'] = $whenArr[1];
			$data['is_order'] = $_POST['is_order'];
			$this->Order_model->orderChange($data);
		}
	}
}
