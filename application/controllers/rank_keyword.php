<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

/**
 * @title
 *
 * @author fangyang (funyung@163.com)
 * @since 2013-4-5 6:24:28
 *       
 */
class Rank_keyword extends CI_Controller
{
	public function __construct()
	{
		parent::__construct ();
		
		$this->load->helper ( array (
				'form',
				'url' 
		) );
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'security' );
		$this->load->library ( 'fx_auth' );
		$this->load->library('table');
		$this->load->model ( 'keyword_rank_model' );
		$this->load->model ( 'Rank_baidu_model' );
		
		if(!$this->fx_auth->is_logged_in())
		{
			redirect('/auth/send_again/');
		}
		
	}
	
	public function index()
	{ 
		static $data = array (),$startdate = '',$endate = '',$status = '';
		if($_GET)
		{
			$po = &$_GET;
			if($po['action'] == 'sall')
			{
				//时间计算
				if (isset ( $po ['startdate'] ) && isset ( $po ['endate'] ))
				{
					$startdate = $po ['startdate'];
					$endate = $po ['endate'];
				} else if (! isset ( $po ['startdate'] ) && isset ( $po ['endate'] ))
				{
					$endate = $po ['endate'];
					$startdate = $endate;
					
				} else if (isset ( $po ['startdate'] ) && ! isset ( $po ['endate'] ))
				{
					$startdate = $po ['startdate'];
					$endate = $startdate;
				}
				
				//需要筛选的字段
				$filterArr = array (
						'uid' => $po['uid'],
						'searchdate >=' => $startdate ,
						'searchdate <=' => $endate ,
				);
				
				//当时间都为空
				if(empty($po ['startdate'] ) &&  empty ( $po ['endate'] ))
				{
					$filterArr = array (
							'uid' => $po['uid'],
					);
				}
				//用户
				if(!empty($po['name']))
				{
					$filterArr['author'] = $po['name'];
					
				}
				
				//占位
			    switch (@$po['status'])
				{
					case 'have' :
						$filterArr['rank !='] = '';
						$status = 'have';
						break;
					case 'none':
						$filterArr['rank ='] = '';
						$status = 'none';
						break;
					default:
						$status = 'all';
				}
				
				$where = $filterArr;
				$data['startdate'] = $startdate;
				$data['endate'] = $endate;
				$data['status'] = $status;
				$data['unit'] = $po['uid'];
				$data['author'] = isset($po['name'])?$po['name']:'';
				$unit = $po['uid'];
				
			}
		}else{
			
			//$unit = $this->uri->uri_to_assoc(3);
			$unit = '1';
			$where = array (
					'uid' => isset($unit['unit'])? $unit['unit']:'1',
					'author' => $this->fx_auth->get_username()
			);
			$data['status'] = 'all';
			$data['unit'] = isset($unit['unit'])? $unit['unit']:'1';
		}		
		
		//分页参数传递
		$startdate = isset ( $po ['startdate'] ) ? $po ['startdate'] : '';
		$endate = isset ( $po ['endate'] ) ? $po ['endate'] : '';
		$name = isset ( $po ['name'] ) ? $po ['name'] : '';
		
		//$uri = $this->uri->segment ( 12 ); //分页所在段
		//$offset = (! empty ( $uri ) && is_numeric ( $uri )) ? $uri : 0;
		$offset = (! empty ( $_GET['per_page'] ) && is_numeric ( $_GET['per_page']  )) ? $_GET['per_page']  : 0;
		$per_page = 50;
		$keyword_data = $this->keyword_rank_model->default_keyword_rank ( $per_page, $offset, $where );
		$total = count ( $this->keyword_rank_model->fetch_all ( $where ) );
	    $data['total'] = $total;
		$data ['keyword_data'] = $keyword_data;
		
		/*
		 * 分页开始
		 */
		$this->load->library ( 'pagination' );
		$config = array ();
		$config['page_query_string'] = TRUE;
		//$config['enable_query_strings'] =TRUE;
		//$config ['uri_segment'] = 6;
		//$config ['base_url'] = site_url ( 'rank_keyword/index/unit/' . $data['unit'].'/startdate/'.$startdate.'/endate/'.$endate.'/status/'.$status );
		$config ['base_url'] = site_url ( 'rank_keyword/index/?action=sall&status='.$status.'&name='.$name.'&uid='.$unit.'&startdate='.$startdate.'&endate='.$endate);
		$config ['total_rows'] = $total;
		$config ['per_page'] = $per_page;
		$config ['full_tag_open'] = '<div class="pagination pagination-small pagination-right"><ul>';
		$config ['full_tag_close'] = '</ul></div>';
		$config ['first_link'] = '首页';
		$config ['last_link'] = '末页';
		$config ['first_tag_open'] = '<li>';
		$config ['first_tag_close'] = '</li>';
		$config ['prev_link'] = '«';
		$config ['prev_tag_open'] = '<li class="prev">';
		$config ['prev_tag_close'] = '</li>';
		$config ['next_link'] = '»';
		$config ['next_tag_open'] = '<li>';
		$config ['next_tag_close'] = '</li>';
		$config ['last_tag_open'] = '<li>';
		$config ['last_tag_close'] = '</li>';
		$config ['cur_tag_open'] = '<li class="active"><a href="#">';
		$config ['cur_tag_close'] = '</a></li>';
		$config ['num_tag_open'] = '<li>';
		$config ['num_tag_close'] = '</li>';
		$config ['num_links'] = 1;
		$config ['use_page_numbers'] = false;
		
		$this->pagination->initialize ( $config );
		
		$data ['hospital_unit'] = $this->keyword_rank_model->getHospitalunit ();
		$data['users'] = $this->users->get_all()->result();
		//已经查询过的关键词
		$data['searched'] = $this->keyword_rank_model->getSearched(@$po['name'],@$po['uid']);
		// 载入视图
		$this->load->view ( 'rank_keyword/index', $data );
		
	}
	
	/**
	 * 	filter
	 */
	public function filter()
	{
		
	}
	
	/**
	 * insert keywords page
	 * @return null
	 */
	public function insertView()
	{
		$data ['hospital_unit'] = $this->keyword_rank_model->getHospitalunit ();
		$this->load->view ( 'rank_keyword/insert', $data );
	}
	
	/**
	 * insert keywords data from index
	 */
	public function insert()
	{
		if (isset ( $_POST ['uid'] ) && isset ( $_POST ['keyword'] ) && isset ( $_POST ['author'] ))
		{
			$keyword = $_POST ['keyword'];
			$edata = array ();
			$edata = explode ( "\n", $keyword );
			$edata = $this->keyword_rank_model->delArrayRepeat ( $edata );
			
			$is_repeat = isset($_POST['is_repeat'])?$_POST['is_repeat']:'';
			foreach ( $edata as $a => $b )
			{
				$data = array (
						'uid' => $_POST ['unit'],
						'keyword' => trim ( $b ),
						'insertdate' => date ( 'Y-m-d H:s:m' ),
						'author' => $this->fx_auth->get_username () 
				);
				//禁止重复   //重复分两种   当前账户重复  与其他账户重复
				if($this->keyword_rank_model->is_exist(trim ( $b ),$is_repeat))
				{
					$this->keyword_rank_model->insert_keyword ( $data );
				}
			}
			redirect('/rank_keyword/');
		} else
		{
			$this->insertView ();
		}
	
	}
	
	/**
	 * delete select data
	 */
	public function delete()
	{
		if (isset ( $_GET ['id'] ))
		{
			$id = $_GET ['id'];
			$data = explode ( "|", $id );
		}
		if ($this->keyword_rank_model->delete_keyword ( $data ))
		{
			echo 'success';
		} else
		{
			echo 'failure';
		}
	}
	
	/**
	 * 百度查询结果
	 */
	function baiduRank()
	{

		static $resultArr=array();
		$unit = $_POST ['unit'];
		$id = $_POST ['id'];
		$keyword = $this->keyword_rank_model->getKeyword ( $id );
		$unit = $this->keyword_rank_model->getUnit ( $unit );
		$this->Rank_baidu_model->search ( $keyword, $unit, $id );
		$resultArr=$this->Rank_baidu_model->rankResult($id,'keyword_rank');
		$backValue=array();
	    $backValue['status']='success';
	    $backValue['id']=$id;
	    $backValue['rank']=$this->indexZhan($resultArr[0]['rank']);
	    $backValue['compete']=$resultArr[0]['compete'];
	   // $backValue['snapshot']='无快照';//$resultArr[0]['snapshot'];
	    $backValue['num_spread']=$this->tuiguang($resultArr[0]['num_spread']);
	    $backValue['searchdate']=$resultArr[0]['searchdate'];
		echo json_encode($backValue);
		
	}
	
	/**
	 * 推广格式
	 * @param string $data
	 */
	function tuiguang($data='')
	{
		static $num_spread,$result;
		$tbaidu = array (
				'上',
				'下',
				'右' 
		);
		$num_spread = $data;
		if (!isset($num_spread))
		{
			$data = '无';
		} else
		{
			$num_spread = explode ( ',', $num_spread );
			foreach ( $num_spread as $v => $n )
			{
				$result .= $tbaidu [$v] . ':<b style="color:#b94a48">' . $n . '</b>&nbsp;';
			}
		}
		
		return $result;
	}
	
	/**
	 * 首页占位格式
	 * @param string $data
	 */
	function indexZhan($data)
	{
		$rank = $data;
		if(empty($rank))
		{
			return  '无';
		}else{
			return $rank.'&nbsp;|&nbsp;<b style="color:#b94a48">'.count(explode(',',$rank)).'</b>';
		}
	}
	
	/**
	 * changesUnit
	 */
	public function changeUnit()
	{
		$unit = $_GET ['unit'];
		
		$keyword_data = $this->keyword_rank_model->default_keyword_rank ( '', '' );
		echo 'success';
		
	}
	
	/**
	 * to excel .
	 * csv
	 */
	public function toCsv()
	{
		static $table = 'keyword_placeholder';
		$this->load->dbutil ();
		$query = $this->db->query ( "SELECT * FROM $table where uid='1'" );
		echo $this->dbutil->csv_from_result ( $query );
	}
}
 