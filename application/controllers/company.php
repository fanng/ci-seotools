<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         公司人员管理/等等其他一些信息/此处的信息独立
 * @author        fangyang (funyung@163.com)
 * @since         2013-5-2 10:20
 *     
 */

class Company extends CI_Controller
{
	
	/**
	 * 构造函数
	 */
	
	public function __construct ()
	{
		parent::__construct();
	
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('security');
		$this->load->library('fx_auth');
		$this->load->library('table');
		$this->load->model('company_model');
	
		if(!$this->fx_auth->is_logged_in())
		{
			redirect('/auth/send_again/');
		}
	}
	
	/**
	 * 
	 */
	function index()
	{
		
	}
	
	/**
	 * 人员概括
	 */
	function personnel()
	{
		$data = array();
		$data['users'] = $this->company_model->fetch_all();
		
		$this->load->view('company/personnel',$data);
	}
	
	/**
	 * 更新员工等等一些信息
	 */
	function add()
	{
		
	}
	
	/**
	 * 添加新员工
	 * 
	 * @return null
	 */
	function insertView()
	{
		$data = $newData = array();
		$data['posts'] = $this->company_model->fetch_all_posts();
		
		if(isset($_POST['submit']))
		{
			$this->form_validation->set_rules('name', '姓名', 'required');
			$this->form_validation->set_rules('telephone', '联系方式', 'required');
			$this->form_validation->set_rules('posts', '职位', 'required');
			$this->form_validation->set_rules('joindate', '入职时间', 'required');
			$this->form_validation->set_rules('address');
			$this->form_validation->set_rules('qq');
			$this->form_validation->set_rules('dormitory');
			$this->form_validation->set_rules('remark');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('company/add',$data);
			}
			else
			{
				//姓名不能重复
				if($this->company_model->verify_name($this->form_validation->set_value('name')) ==TRUE)
				{
					$newData['name'] = $this->form_validation->set_value('name');
					$newData['telephone'] = $this->form_validation->set_value('telephone');
					$newData['post'] = $this->form_validation->set_value('posts');
					$newData['separation_time'] = $this->form_validation->set_value('joindate');
					$newData['address'] = $this->form_validation->set_value('address');
					$newData['qq'] = $this->form_validation->set_value('qq');
					$newData['dormitory'] = $this->form_validation->set_value('dormitory');
					$newData['remark'] = $this->form_validation->set_value('remark');
					
					$this->company_model->insertNew($newData);
					redirect('company/personnel');
				}else
				{
					$data['error'] = '添加失败,姓名不能重复';
					$this->load->view ( 'company/add',$data);
				}
			}
		}else
		{
			$this->load->view ( 'company/add',$data);
		}	
	}
	
	/**
	 * 更新员工信息
	 */
	function update()
	{
		$data = array();
		$data['posts'] = $this->company_model->fetch_all_posts();
		if(isset($_GET['id']))
		{
			$id = $_GET['id'];
			$data['userinfo'] = $this->company_model->getInfo($id);
			
			$this->load->view ( 'company/update',$data);
			
		}else if(isset($_POST['submit'])&&($_POST['action']=='update'))
		{
			$this->form_validation->set_rules('name', '姓名', 'required');
			$this->form_validation->set_rules('telephone', '联系方式', 'required');
			$this->form_validation->set_rules('posts', '职位', 'required');
			$this->form_validation->set_rules('joindate', '入职时间', 'required');
			$this->form_validation->set_rules('address');
			$this->form_validation->set_rules('qq');
			$this->form_validation->set_rules('dormitory');
			$this->form_validation->set_rules('remark');
			

			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('company/update',$data);
			}
			else
			{

				$newData['name'] = $this->form_validation->set_value('name');
				$newData['telephone'] = $this->form_validation->set_value('telephone');
				$newData['post'] = $this->form_validation->set_value('posts');
				$newData['separation_time'] = $this->form_validation->set_value('joindate');
				$newData['address'] = $this->form_validation->set_value('address');
				$newData['qq'] = $this->form_validation->set_value('qq');
				$newData['dormitory'] = $this->form_validation->set_value('dormitory');
				$newData['remark'] = $this->form_validation->set_value('remark');
				
				$this->company_model->updateNew($_POST['id'],$newData);
				redirect('company/personnel');
			}
		}else{
			
			redirect('company/update');
		}
	}

}