<?php

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
/**
 * @title  excel 导入导出
 * 
 * @author fangyang (funyung@163.com)
 * @since  2013-4-15 12:00:38
 *       
 */
class Toexcel extends CI_Controller
{
	public function __construct()
	{
		parent::__construct ();
		$this->load->library ( 'fx_auth' );
		$this->load->library ( 'excel' );
		$this->load->helper ( array (
				'form',
				'url' 
		) );
		$this->load->model (array( 'mexcel','ZhanweiKeyword' ));
		define ( 'EXCELFILE_PATH', 'ExcelFile/' ); // 定义Excel路径
		header ( "Content-type: text/html; charset=utf-8" );
	}
	
	function index()
	{
		//parent::CI_Controller ();
		
	}

    /**
     * 关键词占位查询导出
     *
     * @internal param unknown_type $uid
     */
	function keyword()
	{
		if($_GET)
		{
			$where = array();
			$action = &$_GET;
			$where['uid'] = isset($action['uid'])?$action['uid']:'0';
			$where['author'] = $this->fx_auth->get_username();
			//只导出已经查询过的词
			$where ['searchdate !='] = 'null';
			//导入时间
			if(!empty($action['startdate'])||!empty($action['enddate']))
			{
				$where['insertdate'] = '>='.$action['startdate'];
				$where['insertdate'] = '<='.$action['enddate'];
			}
			//关键词状态 { 0：全部 1：有占位 2：无占位}
			$exportype = isset($action['exportype'])?$action['exportype']:'0';
			switch($exportype)
			{
				case 0 :
					break;
				case 1 :
					$where['rank !=']='';
					break;
				case 2 :
					$where['rank =']='';
					break;
			}
			
			$title = array (
					'关键词',
					'占位',
					'推广位'
			);
			$data = $this->mexcel->writeExcelKeyword ($where);
			$this->excel->writer ( $title, $data ); // 写入excel
		}
	}

    public function urlzhanwei()
    {
        if($_GET)
        {
            $where = array();
            $action = &$_GET;
            $where['uid'] = isset($action['uid'])?$action['uid']:'0';
            $where['author'] = $this->fx_auth->get_username();
            //只导出已经查询过的词
            $where ['searchdate !='] = 'null';
            //导入时间
            if(!empty($action['startdate'])||!empty($action['enddate']))
            {
                $where['insertdate'] = '>='.$action['startdate'];
                $where['insertdate'] = '<='.$action['enddate'];
            }
            //关键词状态 { 0：全部 1：有占位 2：无占位}
            $exportype = isset($action['exportype'])?$action['exportype']:'0';
            switch($exportype)
            {
                case 0 :
                    break;
                case 1 :
                    $where['rank !=']='';
                    break;
                case 2 :
                    $where['rank =']='';
                    break;
            }

            $title = array (
                '关键词',
                '排名个数',
                '排名情况',
                '查询日期：'.date('Y-m-d')
            );

            $data = $this->mexcel->writeExcelKeyword ($where);

            //所有占位
            $sumRank = $this->ZhanweiKeyword->sumRank($where)->rank;

            //有占位
            $hasRank = $this->ZhanweiKeyword->hasRank($where);

            $data[0]['result'] = '搜索引擎：百度';
            $data[1]['result'] = '总：'.count($data);
            $data[2]['result'] = '首页排名总个数：'.$sumRank;
            $data[3]['result'] = '比率：'.round($hasRank/count($data)*100,2).'%';

            $this->excel->writer ( $title, $data ); // 写入excel
        }
    }

    /**
     * url排名查询导出
     *
     * @param int|unknown_type $uid
     */
	function paiMing($uid = 1)
	{
		$title = array (
				'标题',
				'链接',
				'是否收录',
				'快照日期'
		);
		
		$data = $this->mexcel->writeExcelPaiMing ($uid);
		$this->excel->writer ( $title, $data ); // 读取excel
	}
}
 