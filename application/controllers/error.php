<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         错误页
 * @author        fangyang (funyung@163.com)
 * @since         2013-5-3 6:48:05
 *     
 */
 
class Error extends CI_Controller 
{
	public function __construct ()
	{
		parent::__construct();
	
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('security');
		$this->load->library('fx_auth');
		
		/* if(!$this->fx_auth->is_logged_in())
		{
			redirect('/auth/send_again/');
		} */
	
	}
	
	function index()
	{
		parent::__construct();
	}
	
	
    public function ip_not_allow()
    {
        $this->load->view('error/index');
    }
}
 