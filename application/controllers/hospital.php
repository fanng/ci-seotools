<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         
 * @author		    fangyang (funyung@163.com)
 * @since		    2013-4-9 6:52:49
 *     
 */
 
class Hospital extends CI_Controller 
{

	/**
	 * 构造函数
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper ( array (
				'form',
				'url' 
		) );
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'security' );
		$this->load->library ( 'fx_auth' );
		$this->load->library('table');
		
		$this->load->model('Hospital_model');
		$this->load->model('company_model');
		
		$this->table_name = 'hospital_unit';
		
		if(!$this->fx_auth->is_logged_in())
		{
			redirect('/auth/send_again/');
		}
	}
	
	public function unit()
	{
		$data = array();
		$data['units'] = $this->Hospital_model->hospital_unit();
		$this->load->view('hospital/unit',$data);
	}
	
	/**
	 * 添加医院
	 */
	public function add()
	{
		if(isset($_POST['h_title'])&&isset($_POST['siteurl']))
		{
			static $data = array();$dataAll=array();
			$h_title = $_POST ['h_title'];
			$siteurl = $_POST ['siteurl'];
			$u_title = isset ( $_POST ['u_title'] ) ? $_POST ['u_title'] : $h_title;
			
			$data = array (
					'title' => $h_title,
					'siteurl' => $siteurl 
			);
			
			$dataUnit = array (
					
					'siteurl' => $siteurl 
			);
			
			$this->Hospital_model->add_hospital_unit ( $data, $dataUnit );
			
			$this->unit ();
		} else
		{
			
			$this->addView ();
		}	
	}
	
	public function addView()
	{
		$data['project'] = $this->Hospital_model->getHospitalProject();
		
		if(isset($_POST['submit'])&&($_POST['action']=='add'))
		{
			$this->form_validation->set_rules('h_title', '网站名称', 'required');
			$this->form_validation->set_rules('project', '所属项目', 'required');
			$this->form_validation->set_rules('siteurl', '网址', 'required');
			$this->form_validation->set_rules('matchs', '词条', 'required');
			$this->form_validation->set_rules('category');
			$this->form_validation->set_rules('content');
			//表单验证失败
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('hospital/add',$data);
			}
			else
			{
				$newData['h_title'] = $this->form_validation->set_value('h_title');
				$newData['hid'] = $this->form_validation->set_value('project');
				$newData['siteurl'] = $this->form_validation->set_value('siteurl');
				$newData['matchs'] = $this->form_validation->set_value('matchs');
				$newData['category'] = $this->form_validation->set_value('category');
				$newData['content'] = $this->form_validation->set_value('content');
				//存在相同的URL
				if($this->Hospital_model->is_exit($newData['siteurl']))
				{
					$this->Hospital_model->insertNew($newData);
					redirect('hospital/unit');
				}else
				{
					$data['error'] = '添加失败,网站已经存在';
					$this->load->view('hospital/add',$data);
				}
				
			}
		}else {
			
			$this->load->view('hospital/add',$data);
		}
	}
	
	/**
	 * 更新网站以及医院信息
	 * 
	 * @return
	 */
	function update()
	{
		$data = array();
		$data['project'] = $this->Hospital_model->getHospitalProject();
		
		if($this->uri->segment(2)=='update')
		{
			if(isset($_POST['submit'])&&($_POST['action']=='update'))
			{
				$data['hospitalinfo'] = $this->Hospital_model->get_info_byid($_POST['id'])->result();
				
				$this->form_validation->set_rules('id');
				$this->form_validation->set_rules('h_title', '网站名称', 'required');
				$this->form_validation->set_rules('project', '所属项目', 'required');
				$this->form_validation->set_rules('matchs', '词条', 'required');
				$this->form_validation->set_rules('category');
				$this->form_validation->set_rules('content');
				//表单验证失败
				if ($this->form_validation->run() == FALSE)
				{
					$this->load->view('hospital/update',$data);
				}
				else
				{
					$newData['id'] = $this->form_validation->set_value('id');
					$newData['h_title'] = $this->form_validation->set_value('h_title');
					$newData['hid'] = $this->form_validation->set_value('project');
					$newData['matchs'] = $this->form_validation->set_value('matchs');
					$newData['category'] = $this->form_validation->set_value('category');
					$newData['content'] = $this->form_validation->set_value('content');
					
					$this->Hospital_model->updateNew($newData['id'],$newData);
					redirect('hospital/unit');
				}
				
			}else if(isset($_GET['id']))
			{
				$data['hospitalinfo'] = $this->Hospital_model->get_info_byid($_GET['id'])->result();
				$this->load->view('hospital/update',$data);
			}else
			{
				redirect('hospital/unit');
			}
		}else {
			redirect('hospital/unit');
		}	
	}
	
	/**
	 * 
	 */
	function delete()
	{
		if(($this->uri->segment(2)=='delete')&&isset($_GET['id']))
		{
			$this->Hospital_model->delete_byid($_GET['id']);
			redirect('hospital/unit');
		}else 
		{
			redirect('hospital/unit');
		}
	}
}
 