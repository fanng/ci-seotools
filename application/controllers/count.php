<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         外推绩效统计
 * @author        fangyang (funyung@163.com)
 * @since         2013-4-23 10:20
 *     
 */

class Count extends CI_Controller
{
	public function __construct ()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('security');
		$this->load->library('fx_auth');
		$this->load->library('table');
		$this->load->model('fx_auth/users');
		$this->load->model('count_model');
		
	    if(!$this->fx_auth->is_logged_in())
	    {
	    	redirect('/auth/send_again/');
	    }
	}

	public function index()
	{
		$data = array();
        
		$data['users'] = $this->users->get_all()->result();
		$this->load->view('count/index',$data);
	}
}
