<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         
 * @author		    fangyang (funyung@163.com)
 * @since		    2013-4-10 ����6:13:03
 *     
 */
 
class Config extends CI_Controller 
{
    
	public function __construct ()
	{
		parent::__construct();
	
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('security');
		$this->load->library('fx_auth');
		
		if(!$this->fx_auth->is_logged_in())
		{
			redirect('/auth/send_again/');
		}
	
	}
	
    public function index()
	{
		$data['config']['title'] = '系统设置';
	    $this->load->view('config/index',$data);
	}
}
 