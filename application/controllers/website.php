<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         公司网站/后台FTP
 * @author        fangyang (funyung@163.com)
 * @since         2013-5-6 10:20
 *     
 */

class Website extends CI_Controller
{

	/**
	 * 构造函数
	 */
	public function __construct ()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('security');
		$this->load->library('fx_auth');
		$this->load->library('table');
		$this->load->model('Website_model');
		$this->load->model('Hospital_model');

		if(!$this->fx_auth->is_logged_in())
		{
			redirect('/auth/send_again/');
		}
	}
	
	/**
	 * 后台账户以及FTP
	 */
	function backstage_ftp()
	{
		$data = array();
		$data['config']['title'] = '后台/FTP管理';
		$data['project'] = $this->Hospital_model->getHospitalProject();
		$data['site'] = $this->Website_model->getAllSite();
		$data['type'] = $this->Website_model->getBackstageType();
		$this->load->view('website/backstage_ftp',$data);
	}
	
	/**
	 * 添加网站
	 */
	function insertView()
	{
		$data = $newData = array();
		$data['site'] = $this->Website_model->getAllSite();
		$data['type'] = $this->Website_model->getBackstageType();
		$data['project'] = $this->Hospital_model->getHospitalProject();
		
		if(isset($_POST['submit']))
		{
			$this->form_validation->set_rules('url', '网址', 'required');
			$this->form_validation->set_rules('title', '网站名称', 'required');
			$this->form_validation->set_rules('status', '状态', 'required');
			$this->form_validation->set_rules('pid');
			$this->form_validation->set_rules('b_address');
			$this->form_validation->set_rules('h_address');
			$this->form_validation->set_rules('b_name');
			$this->form_validation->set_rules('b_password');
			$this->form_validation->set_rules('ftp_name');
			$this->form_validation->set_rules('ftp_password');
			$this->form_validation->set_rules('b_type');
				
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('website/add',$data);
			}
			else
			{
				//姓名不能重复
				if($this->Website_model->verify($this->form_validation->set_value('url')) ==TRUE)
				{
					$newData['title'] = $this->form_validation->set_value('title');
					$newData['url'] = $this->form_validation->set_value('url');
					$newData['pid'] = $this->form_validation->set_value('pid');
					$newData['b_address'] = $this->form_validation->set_value('b_address');
					$newData['h_address'] = $this->form_validation->set_value('h_address');
					$newData['b_name'] = $this->form_validation->set_value('b_name');
					$newData['b_password'] = $this->form_validation->set_value('b_password');
					$newData['ftp_name'] = $this->form_validation->set_value('ftp_name');
					$newData['ftp_password'] = $this->form_validation->set_value('ftp_password');
					$newData['status'] = $this->form_validation->set_value('status');
					$newData['b_type'] = $this->form_validation->set_value('b_type');
						
					$this->Website_model->insertNew($newData);
					redirect('website/backstage_ftp');
				}else
				{
					$data['error'] = '添加失败,网站已经存在';
					$this->load->view ( 'website/add',$data);
				}
			}
		}else
		{
			$this->load->view ( 'website/add',$data);
		}
	}
	
	
	/**
	 * 添加域名
	 */
	function domainAdd()
	{
		$data = $newData = array();
		$data['site'] = $this->Website_model->getAllSite();
		$data['type'] = $this->Website_model->getBackstageType();
		$data['project'] = $this->Hospital_model->getHospitalProject();
		
		if(isset($_POST['submit']))
		{
			$this->form_validation->set_rules('url', '网址', 'required');
			$this->form_validation->set_rules('title', '网站名称', 'required');
			$this->form_validation->set_rules('status', '状态', 'required');
			$this->form_validation->set_rules('pid');
			$this->form_validation->set_rules('b_address');
			$this->form_validation->set_rules('h_address');
			$this->form_validation->set_rules('b_name');
			$this->form_validation->set_rules('b_password');
			$this->form_validation->set_rules('ftp_name');
			$this->form_validation->set_rules('ftp_password');
			$this->form_validation->set_rules('b_type');
		
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('website/domain_add',$data);
			}
			else
			{
				//姓名不能重复
				if($this->Website_model->verify($this->form_validation->set_value('url')) ==TRUE)
				{
					$newData['title'] = $this->form_validation->set_value('title');
					$newData['url'] = $this->form_validation->set_value('url');
					$newData['pid'] = $this->form_validation->set_value('pid');
					$newData['b_address'] = $this->form_validation->set_value('b_address');
					$newData['h_address'] = $this->form_validation->set_value('h_address');
					$newData['b_name'] = $this->form_validation->set_value('b_name');
					$newData['b_password'] = $this->form_validation->set_value('b_password');
					$newData['ftp_name'] = $this->form_validation->set_value('ftp_name');
					$newData['ftp_password'] = $this->form_validation->set_value('ftp_password');
					$newData['status'] = $this->form_validation->set_value('status');
					$newData['b_type'] = $this->form_validation->set_value('b_type');
		
					$this->Website_model->insertNew($newData);
					redirect('website/domain');
				}else
				{
					$data['error'] = '添加失败,网站已经存在';
					$this->load->view ( 'website/domain_add',$data);
				}
			}
		}else
		{
			$this->load->view ( 'website/domain_add',$data);
		}
	}
	
	/**
	 * 更新网站信息
	 */
	function update()
	{
		$data = array();
		$data['type'] = $this->Website_model->getBackstageType();
		$data['project'] = $this->Hospital_model->getHospitalProject();
		if(isset($_GET['id']))
		{
			$id = $_GET['id'];
			$data['webinfo'] = $this->Website_model->getInfo($id);
				
			$this->load->view ( 'website/update',$data);
				
		}else if(isset($_POST['submit'])&&($_POST['action']=='update'))
		{
			$this->form_validation->set_rules('url', '网址', 'required');
			$this->form_validation->set_rules('title', '网站名称', 'required');
			$this->form_validation->set_rules('status', '状态', 'required');
			$this->form_validation->set_rules('pid');
			$this->form_validation->set_rules('b_address');
			$this->form_validation->set_rules('h_address');
			$this->form_validation->set_rules('b_name');
			$this->form_validation->set_rules('b_password');
			$this->form_validation->set_rules('ftp_name');
		    $this->form_validation->set_rules('ftp_password');
			$this->form_validation->set_rules('b_type');
	
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('website/update',$data);
			}
			else
			{
	
				$newData['title'] = $this->form_validation->set_value('title');
				$newData['url'] = $this->form_validation->set_value('url');
				$newData['pid'] = $this->form_validation->set_value('pid');
				$newData['b_address'] = $this->form_validation->set_value('b_address');
				$newData['h_address'] = $this->form_validation->set_value('h_address');
				$newData['b_name'] = $this->form_validation->set_value('b_name');
				$newData['b_password'] = $this->form_validation->set_value('b_password');
				$newData['ftp_name'] = $this->form_validation->set_value('ftp_name');
				$newData['ftp_password'] = $this->form_validation->set_value('ftp_password');
				$newData['status'] = $this->form_validation->set_value('status');
				$newData['b_type'] = $this->form_validation->set_value('b_type');
	
				$this->Website_model->updateNew($_POST['id'],$newData);
				redirect('website/backstage_ftp');
			}
		}else{
				
			redirect('website/update');
		}
	}
	
	/**
	 * 域名管理
	 * 
	 */
	function domain()
	{
		$data = array();
		$data['config']['title'] = '域名管理';
		$data['type'] = $this->Website_model->getBackstageType();
		$data['project'] = $this->Hospital_model->getHospitalProject();
		$data['site'] = $this->Website_model->getAllDomain();
		$this->load->view('website/domain',$data);
	}
	
	/**
	 * 基本信息配置
	 * 
	 */
	function basic()
	{
		$data = array();
		$data['config']['title'] = '域名管理';
		$this->load->view('website/basic',$data);
	}
}