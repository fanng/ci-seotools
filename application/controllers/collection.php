<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         网址采集
 * @author        fangyang (funyung@163.com)
 * @since         2013-6-2 10:20
 *     
 */

class Collection extends CI_Controller
{
	
	/**
	 * 构造函数
	 */
	public $data;
	
	public function __construct ()
	{
		parent::__construct();
	
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('security');
		$this->load->library('fx_auth');
		$this->load->library('table');
	    $this->load->model('collection_model');
	    
	    $this->data['config']['title'] = '网址采集';
	    
		if(!$this->fx_auth->is_logged_in())
		{
			redirect('/auth/send_again/');
		}
	}
	
	/**
	 * 
	 */
	function index()
	{
		$data = array();
		$this->load->view('collection/index',$this->data);
	}
	
	/**
	 * 采集网址以及标题
	 */
	function search_engine()
	{
		$this->form_validation->set_rules('keys', '关键词', 'required');
		
		if ($this->form_validation->run() == TRUE)
		{
			$keys = $this->input->post('keys');
			$pages = $this->input->post('pages');
			
			$this->collection_model->search_baidu($keys,$pages);
			
			$this->load->view('collection/index',$this->data);
		}
		else
		{
			$this->load->view('collection/index',$this->data);
		}
		
		
	}
}