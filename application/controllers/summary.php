<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title          index summary and systeminfo 
 * @author         fangyang (funyung@163.com)
 * @since          2013-4-5 5:54:13
 *     
 */
 
class Summary extends CI_Controller 
{

    public function __construct()
	{
		parent::__construct ();
		
		$this->load->helper ( array (
				'form',
				'url' 
		) );
		// $this->load->library ( 'form_validation' );
		$this->load->library ( 'security' );
		$this->load->library ( 'fx_auth' );
		
		if ($this->fx_auth->is_logged_in ())
		{ // logged in
			$this->load->view('summary/index',array('username'=>($this->fx_auth->get_username())));
		}else
		{
			redirect('/auth/login/');
		}
	}
    
    public function index()
    {
    
       
        //$this->load->view('summary/index');
    }
}
 