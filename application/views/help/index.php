<?php $this->load->view('inc/head'); ?>
<style>
section{padding-top: 60px;}
</style>
<div class="container">
	<header class="jumbotron subhead" id="overview">
		<h1>外推管理系统帮助中心</h1>
		<div class="subnav">
			<ul class="nav nav-pills">
				<li class="active"><a href="#javascript">摘要</a></li>
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">关键词查询 <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#navs">关键词导入 </a></li>
						<li><a href="#navbar">竞争度 </a></li>
						<li><a href="#breadcrumbs">最新快照 </a></li>
						<li><a href="#pagination">首页占位 </a></li>
						<li><a href="#pagination">推广位数量 </a></li>
						<li><a href="#pagination">操作 </a></li>
					</ul></li>
				<li class=""><a href="#javascript">关键词排名</a></li>
			</ul>
		</div>
	</header>
    
   <!-- Content start --> 
   
   <section id="buttonGroups">
		<div class="page-header">
			<h2>
				关键词的导入
			</h2>
		</div>
		<div class="row">
			<div class="span12">
				<div class="well">
    				<p>竞争度=<code>(上/8+下/3+右/8)/3</code> *关于竞争度的算法在不断改进,已达到更准确的效果。</p>
    				<p>竞争度的范围从0到100,如果所有推广位都满的话，那么竞争度就为100，反之为0。</p>
				</div>
			</div>
		</div>
	</section>
	
	<section id="buttonGroups">
		<div class="page-header">
			<h2>
				竞争度
			</h2>
		</div>
		<div class="row">
			<div class="span12">
				<div class="well">
    				<p>竞争度=<code>(上/8+下/3+右/8)/3</code> *关于竞争度的算法在不断改进,已达到更准确的效果。</p>
    				<p>竞争度的范围从0到100,如果所有推广位都满的话，那么竞争度就为100，反之为0。</p>
				</div>
			</div>
		</div>
	</section>
	<!-- Content end --> 

</div>
<?php $this->load->view('inc/foot');?>