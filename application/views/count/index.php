<?php $this->load->view('inc/head'); ?>
<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>外推绩效统计</legend>
	</header>
<?php

//日期处理
$date = date("Ym");
$date_time = strtotime(substr($date, 0, 4) . "-" . substr($date, 4, 2) . "-01 0:0:0");
$y_array = $m_array = $d_array = array ();
for($i = date ( "Y" ); $i >= (date ( "Y" ) - 2); $i --)
	$y_array [] = $i;
for($i = 1; $i <= 12; $i ++)
	$m_array [] = $i;
for($i = 1; $i <= 31; $i ++)
{
	if ($i <= 28 || checkdate ( date ( "n", $date_time ), $i, date ( "Y", $date_time ) ))
	{
		$d_array [] = $i;
	}
}

//$db = "SELECT COUNT(*) FROM `$table` WHERE date >= AND date <= AND author = ";

//表格模版	
$tmpl = array ( 
		'table_open' => '<table class="table table-condensed table-striped">', 
);
$this->table->set_template ( $tmpl );

//表头
$headArray= array();
$headArray[] = '日期';

foreach($users as $user){
	$headArray[].= $user->username;
}
$this->table->set_heading($headArray);

//表格主体部分
$data = $mdate = $finaldata=array();
foreach ( $d_array as $r =>$i)
{
	unset($data);
	foreach ($users as $r=>$name)
	{
		$data[]=$this->count_model->jixiao($name->username,date ( "Y", $date_time ) . '-' .date ( "m", $date_time ) . '-' . $i)->num_rows();
	}
	
	@$rrdata [] = $data;
}

foreach ( $d_array as $r=>$i )
{
	@$finaldata[] = array_merge(array(date ( "n", $date_time ) . '-' . $i),$rrdata[$r]);
}

$this->table->add_row('汇总', 'Wednesday', 'Express');

echo $this->table->generate($finaldata);
?>
	
<?php $this->load->view('inc/foot');?>