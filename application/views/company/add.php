<?php $this->load->view('inc/head'); ?>

<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<!-- <div class="page-header">
		</div> -->
		<legend>添加新的员工</legend>
	</header>
	<?php 
	$this->form_validation->set_error_delimiters('<span class="help-inline">','</span>');
	$attributes =array('class' => 'form-horizontal', 'name' => 'iform');
	$postoptions = array();
	
	$postoptions[''] = '';
	foreach ($posts as $r =>$p)
	{
		$postoptions[$p->id] = $p->post_name;
	}
	?>
	<?php if(isset($error)):?>
	<div class="alert alert-info">
	    <a class="close" data-dismiss="alert" href="#">×</a>
        <strong>警告！</strong> <?php echo $error;?>
    </div>
    <?php endif?>
	<!-- insert table -->
	<div class="">
		<?php echo form_open($this->uri->uri_string(), $attributes);?>
			<div class="control-group <?php echo form_error('name')==''?'':'error';?>">
				<label class="control-label">姓名</label>
				<div class="controls">
					<input type="text" name="name" value="<?php echo set_value('name'); ?>"/>
					<?=form_error('name') ?>
				</div>
			</div>
			<div class="control-group <?php echo form_error('telephone')==''?'':'error';?>">
				<label class="control-label">联系方式</label>
				<div class="controls">
					<input type="text" name="telephone" value="<?php echo set_value('telephone'); ?>"/>
					<?=form_error('telephone') ?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">地址</label>
				<div class="controls">
					<input type="text" name="address" value="<?php echo set_value('address'); ?>"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">QQ</label>
				<div class="controls">
					<input type="text" name="qq" value="<?php echo set_value('qq'); ?>"/>
				</div>
			</div>
			<div class="control-group <?php echo form_error('posts')==''?'':'error';?>">
				<label class="control-label">职位</label>
				<div class="controls">
					<?php echo form_dropdown('posts',$postoptions)?>
					<?=form_error('posts') ?>
				</div>
			</div>
			<div class="control-group <?php echo form_error('joindate')==''?'':'error';?>">
				<label class="control-label">入职时间</label>
				<div class="controls">
					<div id="joindate" class="input-append left" >
						<input data-format="yyyy-MM-dd" type="text"  name="joindate" value="<?php echo set_value('joindate'); ?>">
						<span class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"> </i>
						</span>
					</div>
					<?=form_error('joindate') ?>
				</div>
				<script type="text/javascript">
				  $(function() {
				    $('#joindate').datetimepicker({
				      pickTime: false
				    });
				  });
				</script>
			</div>
			<div class="control-group">
				<label class="control-label">宿舍</label>
				<div class="controls">
					<input type="text" name="dormitory" value="<?php echo set_value('dormitory'); ?>"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">备注</label>
				<div class="controls">
					<textarea name="remark"><?php echo set_value('remark'); ?></textarea>
				</div>
			</div>
			<div class="control-group form-actions">
			    <button type="button" onclick="history.back()" class="btn">返回</button>
				<button type="reset" class="btn">重置</button>
				<button type="submit" name="submit" class="btn btn-primary">添加</button>
			</div>
		</form>
	</div>
	<!-- insert table end -->
				
</div>	
<?php $this->load->view('inc/foot'); ?>