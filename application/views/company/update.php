<?php $this->load->view('inc/head'); ?>

<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>更新信息：<b style="color:#bd362f"><?php echo $userinfo[0]->name;?></b></legend>
	</header>
	<?php 
	$this->form_validation->set_error_delimiters('<span class="help-inline">','</span>');
	$attributes =array('class' => 'form-horizontal', 'name' => 'iform');
	$postoptions = array();
	
	$postoptions[''] = '';
	foreach ($posts as $r =>$p)
	{
		$postoptions[$p->id] = $p->post_name;
	}
	?>
	<?php if(isset($error)):?>
	<div class="alert alert-info">
	    <a class="close" data-dismiss="alert" href="#">×</a>
        <strong>警告！</strong> <?php echo $error;?>
    </div>
    <?php endif?>
	<!-- insert table -->
	<?php foreach($userinfo as $r):?>
	<div class="">
		<?php echo form_open($this->uri->uri_string(), $attributes);?>
		    <div class="control-group">
				<label class="control-label">编号</label>
				<div class="controls">
					<input type="text" value="<?php echo $r->id; ?>" disabled="disabled"/>
				</div>
			</div>
			<div class="control-group <?php echo form_error('name')==''?'':'error';?>">
				<label class="control-label">姓名</label>
				<div class="controls">
					<input type="text" name="name" value="<?php echo $r->name; ?>"/>
					<?=form_error('name') ?>
				</div>
			</div>
			<div class="control-group <?php echo form_error('telephone')==''?'':'error';?>">
				<label class="control-label">联系方式</label>
				<div class="controls">
					<input type="text" name="telephone" value="<?php echo $r->telephone; ?>"/>
					<?=form_error('telephone') ?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">地址</label>
				<div class="controls">
					<input type="text" name="address" value="<?php echo $r->address; ?>"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">QQ</label>
				<div class="controls">
					<input type="text" name="qq" value="<?php echo $r->qq; ?>"/>
				</div>
			</div>
			<div class="control-group <?php echo form_error('posts')==''?'':'error';?>">
				<label class="control-label">职位</label>
				<div class="controls">
					<?php echo form_dropdown('posts',$postoptions,$r->post)?>
					<?=form_error('posts') ?>
				</div>
			</div>
			<div class="control-group <?php echo form_error('joindate')==''?'':'error';?>">
				<label class="control-label">入职时间</label>
				<div class="controls">
					<div id="joindate" class="input-append left" >
						<input data-format="yyyy-MM-dd" type="text"  name="joindate" value="<?php echo $r->separation_time; ?>">
						<span class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"> </i>
						</span>
					</div>
					<?=form_error('joindate') ?>
				</div>
				<script type="text/javascript">
				  $(function() {
				    $('#joindate').datetimepicker({
				      pickTime: false
				    });
				  });
				</script>
			</div>
			<div class="control-group <?php echo form_error('joindate')==''?'':'error';?>">
				<label class="control-label">离职时间</label>
				<div class="controls">
					<div id="entry_time" class="input-append left" >
						<input data-format="yyyy-MM-dd" type="text"  name="entry_time" value="<?php echo $r->entry_time; ?>">
						<span class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"> </i>
						</span>
					</div>
					<?=form_error('entry_time') ?>
				</div>
				<script type="text/javascript">
				  $(function() {
				    $('#entry_time').datetimepicker({
				      pickTime: false
				    });
				  });
				</script>
			</div>
			<div class="control-group">
				<label class="control-label">宿舍</label>
				<div class="controls">
					<input type="text" name="dormitory" value="<?php echo $r->dormitory; ?>"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">备注</label>
				<div class="controls">
					<textarea name="remark"><?php echo $r->remark; ?></textarea>
				</div>
			</div>
			<div class="control-group form-actions">
			    <button type="button" onclick="history.back()" class="btn">返回</button>
				<button type="reset" class="btn">重置</button>
				<input type="hidden" name="action" value="update"/>
				<input type="hidden" name="id" value="<?php echo $r->id; ?>"/>
				<button type="submit" name="submit" class="btn btn-primary">更新</button>
			</div>
		</form>
	</div>
	<?php endforeach;?>
	<!-- insert table end -->
				
</div>	
<?php $this->load->view('inc/foot'); ?>