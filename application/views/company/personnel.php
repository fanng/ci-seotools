<?php $this->load->view('inc/head'); ?>
<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>公司员工/通讯录</legend>
	</header>

	<div class="row-fluid show-grid">
		<!-- LEFT BUTTON START -->
		<div class="span4">
			<div class="left">
				<button type="button" href="javascript:void(0)"
					onclick="SelectAll()" class="btn btn-small">全选</button>
				<button class="btn btn-small" type="button"
					onclick="location='/index.php/toexcel/write?uid=1'">导出到excel</button>
				<button onclick="location.href='/index.php/company/insertView'"
					class="btn btn-small btn-info" type="button">添加</button>
				<button type="button" href="javascript:void(0)" onclick="delItem()"
					class="btn btn-small btn-danger">删除</button>
			</div>
		</div>
		<!-- LEFT BUTTON END -->
	</div>
				
<?php 
$tmpl = array (
		'table_open' => '<table class="table table-condensed table-striped">',
);
$this->table->set_heading('选','编号','姓名','联系电话','QQ','地址','职位','入职时间','离职时间','宿舍', '备注', '操作' );

foreach ( $users as $r => $user )
{
	$this->table->add_row ( '<input type="checkbox">',
           $user->id, 
           $user->name, 
           $user->telephone,
           $user->qq,
           $user->address,
           $this->company_model->selectPost($user->post),
           $user->separation_time,
           $user->entry_time,
           $user->dormitory,
           $user->remark,
           '<a href="javascript:void(0)" onclick="location=\'/index.php/company/update?id='.$user->id.'\'"><i class="icon-edit"></i></a>'
    );
}

$this->table->set_template ( $tmpl );
echo $this->table->generate();

?>
</div>
<?php $this->load->view('inc/foot');?>	