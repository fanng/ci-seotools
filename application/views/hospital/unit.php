<?php $this->load->view('inc/head'); ?>
<div class="row-fluid">
	<legend>医院/单元/词条管理</legend>
	<?php echo @$result;?>
	<?php
	$tmpl = array ( 
			'table_open' => '<table class="table table-condensed table-striped">' 
	);
	
	$this->table->set_heading('选','序号','所属项目','网站名','网址','匹配词条','分类','备注','操作' );
	$cell = array('data' => 'Blue', 'class' => 'highlight', 'colspan' => 2);
	foreach ( $units as $r => $unit )
	{
		$this->table->add_row ( 
				array('data' =>'<input type="checkbox">' ,'width' => '3%'),
				array('data' =>$unit->id ,'width' => '3%'),
				array('data' =>$this->Hospital_model->get_project_byid($unit->hid) ,'width' => '10%'),
				array('data' =>$unit->h_title ,'width' => '10%'),
				array('data' =>$unit->siteurl ,'width' => '10%'),
				$unit->matchs,
				$unit->category,
				$unit->content,
				'<div class="btn-group">
                    <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown">选项<span class="caret"></span></button>
                    <ul class="dropdown-menu">
	                    <li><a href="update?id='.$unit->id .'">编辑</a></li>
                        <li><a href="delete?id='.$unit->id .'">删除</a></li>
                    </ul>
                </div>'
		     );
	}
	
	$this->table->set_template ( $tmpl );
	echo $this->table->generate();
	?>
	
	<form class="form-horizontal">
		<div class="form-actions">
			<button type="button" class="btn btn-primary"
				onclick="location.href='/index.php/hospital/addView'">添加</button>
			<button class="btn">返回</button>
		</div>
	</form>



</div>

<?php $this->load->view('inc/foot');?>