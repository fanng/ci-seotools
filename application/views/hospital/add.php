<?php $this->load->view('inc/head'); ?>
<?php 
$this->form_validation->set_error_delimiters('<span class="help-inline">','</span>');
$attributes =array('class' => 'form-horizontal', 'name' => 'iform');
$projectoptions[''] = '';
foreach(@$project as $r =>$p)
{
	$projectoptions[$p->id] = $p->name;
}

?>
<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<!-- <div class="page-header">
		</div> -->
		<legend>添加新的医院</legend>
	</header>
	<?php if(isset($error)):?>
	<div class="alert alert-info">
	    <a class="close" data-dismiss="alert" href="#">×</a>
        <strong>警告！</strong> <?php echo $error;?>
    </div>
    <?php endif?>
    
	<!-- insert table -->
	<div class="">
		<?php echo form_open($this->uri->uri_string(), $attributes);?>
			<div class="control-group <?php echo form_error('h_title')==''?'':'error';?>"">
				<label class="control-label">网站名称</label>
				<div class="controls">
					<input type="text" name="h_title"/>
					<?=form_error('h_title') ?>
				</div>
			</div>
			<div class="control-group <?php echo form_error('siteurl')==''?'':'error';?>"">
				<label class="control-label">医院网站</label>
				<div class="controls">
					<input type="text" name="siteurl"/>
					<?=form_error('siteurl') ?>不需要添加 http://
				</div>
			</div>
			<div class="control-group <?php echo form_error('project')==''?'':'error';?>">
				<label class="control-label">所属项目</label>
				<div class="controls">
					<?php echo form_dropdown('project',$projectoptions)?>
					<?=form_error('project') ?>
				</div>
			</div>
			<div class="control-group <?php echo form_error('matchs')==''?'':'error';?>"">
				<label class="control-label">匹配词条</label>
				<div class="controls">
					<input type="text" name="matchs"/>
					<?=form_error('matchs') ?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">分类</label>
				<div class="controls">
					<textarea name="category"></textarea>
					用英文逗号隔开
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">备注</label>
				<div class="controls">
					<textarea name="content"></textarea>
				</div>
			</div>
			<input type="hidden" name="author" value="<?php echo $this->fx_auth->get_username();?>" />
			<input type="hidden" name="action" value="add"	>
			<div class="control-group form-actions">
			    <button type="button" class="btn" onclick="history.back()">返回</button>
				<button type="reset" class="btn">重置</button>
				<button type="submit" name="submit" class="btn btn-primary">添加</button>
			</div>
		</form>
	</div>
	<!-- insert table end -->
				
</div>	

<?php $this->load->view('inc/foot'); ?>