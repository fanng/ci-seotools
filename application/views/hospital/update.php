<?php $this->load->view('inc/head'); ?>

<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>更新信息：<b style="color:#bd362f"><?php echo @isset($hospitalinfo[0]->h_title)?@$hospitalinfo[0]->h_title:$h_title;?></b></legend>
	</header>
	<?php 
	$this->form_validation->set_error_delimiters('<span class="help-inline">','</span>');
	$attributes =array('class' => 'form-horizontal', 'name' => 'iform');
	$projectoptions[''] = '';
	foreach(@$project as $r =>$p)
	{
		$projectoptions[$p->id] = $p->name;
	}
	?>
	<?php if(isset($error)):?>
	<div class="alert alert-info">
	    <a class="close" data-dismiss="alert" href="#">×</a>
        <strong>警告！</strong> <?php echo $error;?>
    </div>
    <?php endif?>
	<!-- insert table -->
	<?php foreach($hospitalinfo as $r):?>
	<div class="">
		<?php echo form_open($this->uri->uri_string(), $attributes);?>
		    <div class="control-group">
				<label class="control-label">编号</label>
				<div class="controls">
					<input type="text" value="<?php echo $r->id; ?>" disabled="disabled"/>
				</div>
			</div>
			<div class="control-group <?php echo form_error('h_title')==''?'':'error';?>">
				<label class="control-label">网站名称</label>
				<div class="controls">
					<input type="text" name="h_title" value="<?php echo $r->h_title; ?>"/>
					<?=form_error('h_title') ?>
				</div>
			</div>
			<div class="control-group <?php echo form_error('project')==''?'':'error';?>">
				<label class="control-label">所属项目</label>
				<div class="controls">
					<?php echo form_dropdown('project',$projectoptions,$r->hid)?>
					<?=form_error('project') ?>
				</div>
			</div>
			<div class="control-group <?php echo form_error('siteurl')==''?'':'error';?>">
				<label class="control-label">网址</label>
				<div class="controls">
					<input type="text"  value="<?php echo $r->siteurl; ?>" disabled/>
					<?=form_error('siteurl') ?>
				</div>
			</div>
			<div class="control-group" <?php echo form_error('matchs')==''?'':'matchs';?>>
				<label class="control-label">用于匹配的词条</label>
				<div class="controls">
					<input type="text" name="matchs" value="<?php echo $r->matchs; ?>"/>
					<?=form_error('matchs') ?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">分类</label>
				<div class="controls">
					<textarea name="category"><?php echo $r->category; ?></textarea>
				</div>
			</div>
			<div class="control-group form-actions">
			    <button type="button" onclick="history.back()" class="btn">返回</button>
				<button type="reset" class="btn">重置</button>
				<input type="hidden" name="action" value="update"/>
				<input type="hidden" name="id" value="<?php echo $r->id; ?>"/>
				<button type="submit" name="submit" class="btn btn-primary">更新</button>
			</div>
		</form>
	</div>
	<?php endforeach;?>
	<!-- insert table end -->
				
</div>	
<?php $this->load->view('inc/foot'); ?>