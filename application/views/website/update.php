<?php $this->load->view('inc/head'); ?>

<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>更新信息:<b style="color:#bd362f"><?php echo $webinfo[0]->title;?></b></legend>
	</header>
	<?php 
	$this->form_validation->set_error_delimiters('<span class="help-inline">','</span>');
	$attributes =array('class' => 'form-horizontal', 'name' => 'iform');
	$postoptions = array();
	
	$typeoptions[''] = '';
	$projectoptions[''] = '';
	foreach (@$type as $r =>$p)
	{
		$typeoptions[$p->id] = $p->name;
	}
	unset($r,$p);
	foreach(@$project as $r =>$p)
	{
		$projectoptions[$p->id] = $p->name;
	}
	$statusoptions = array('在线','离线','维护');
	?>
	<?php if(isset($error)):?>
	<div class="alert alert-info">
	    <a class="close" data-dismiss="alert" href="#">×</a>
        <strong>警告！</strong> <?php echo $error;?>
    </div>
    <?php endif?>
	<!-- insert table -->
	<?php foreach($webinfo as $r):?> 
	<div class="">
		<?php echo form_open($this->uri->uri_string(), $attributes);?>
		    <div class="control-group">
				<label class="control-label">编号</label>
				<div class="controls">
					<input type="text" value="<?php echo $r->id; ?>" disabled="disabled"/>
				</div>
			</div>
			<div class="control-group <?php echo form_error('url')==''?'':'error';?>">
				<label class="control-label">网址</label>
				<div class="controls">
					<input type="text" name="url" value="<?php echo $r->url; ?>"/>
					<?=form_error('url') ?>
				</div>
			</div>
			<div class="control-group <?php echo form_error('title')==''?'':'error';?>">
				<label class="control-label">网站标题</label>
				<div class="controls">
					<input type="text" name="title" value="<?php echo $r->title; ?>"/>
					<?=form_error('title') ?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">所属项目</label>
				<div class="controls">
					<?php echo form_dropdown('pid',$projectoptions,$r->pid)?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">后台地址</label>
				<div class="controls">
					<input type="text" name="b_address" value="<?php echo $r->b_address; ?>"/>
				</div>
			</div>
			<div class="control-group <?php echo form_error('posts')==''?'':'error';?>">
				<label class="control-label">主机地址</label>
				<div class="controls">
					<input type="text" name="h_address" value="<?php echo $r->h_address; ?>"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">后台用户名</label>
				<div class="controls">
					<input type="text" name="b_name" value="<?php echo $r->b_name; ?>"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">后台密码</label>
				<div class="controls">
					<input type="text" name="b_password" value="<?php echo $r->b_password; ?>"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">FTP用户名</label>
				<div class="controls">
					<input type="text" name="ftp_name" value="<?php echo $r->ftp_name; ?>"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">FTP密码</label>
				<div class="controls">
					<input type="text" name="ftp_password" value="<?php echo $r->ftp_password; ?>"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">后台类型</label>
				<div class="controls">
					<?php echo form_dropdown('b_type',$typeoptions,$r->b_type)?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">状态</label>
				<div class="controls">
					<?php echo form_dropdown('status',$statusoptions,$r->status)?>
					<?=form_error('status') ?>
				</div>
			</div>
		
			<div class="control-group">
				<label class="control-label">备注</label>
				<div class="controls">
					<textarea name="remark"><?php echo $r->remark; ?></textarea>
				</div>
			</div>
			<div class="control-group form-actions">
			    <button type="button" onclick="history.back()" class="btn">返回</button>
				<button type="reset" class="btn">重置</button>
				<input type="hidden" name="action" value="update"/>
				<input type="hidden" name="id" value="<?php echo $r->id; ?>"/>
				<button type="submit" name="submit" class="btn btn-primary">更新</button>
			</div>
		</form>
	</div>
	<?php endforeach;?>
	<!-- insert table end -->
				
</div>	
<?php $this->load->view('inc/foot'); ?>