<?php $this->load->view('inc/head'); ?>

<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>添加新的域名</legend>
	</header>
	<?php 
	$this->form_validation->set_error_delimiters('<span class="help-inline">','</span>');
	$attributes =array('class' => 'form-horizontal', 'name' => 'iform');
	$postoptions = array();
	
	$typeoptions[''] = '';
	$projectoptions[''] = '';
	foreach (@$type as $r =>$p)
	{
		$typeoptions[$p->id] = $p->name;
	}
	unset($r,$p);
	foreach(@$project as $r =>$p)
	{
		$projectoptions[$p->id] = $p->name;
	}
	$statusoptions = array('在线','离线','维护');
	?>
	<?php if(isset($error)):?>
	<div class="alert alert-info">
	    <a class="close" data-dismiss="alert" href="#">×</a>
        <strong>警告！</strong> <?php echo $error;?>
    </div>
    <?php endif?>
	<!-- insert table -->
	<div class="">
		<?php echo form_open($this->uri->uri_string(), $attributes);?>
			<div class="control-group <?php echo form_error('url')==''?'':'error';?>">
				<label class="control-label">域名</label>
				<div class="controls">
					<input type="text" name="url" value="<?php echo set_value('url'); ?>"/>
					<?=form_error('name') ?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">所属项目</label>
				<div class="controls">
					<?php echo form_dropdown('pid',$projectoptions)?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">后台地址</label>
				<div class="controls">
					<input type="text" name="b_address" value="<?php echo set_value('b_address'); ?>"/>
				</div>
			</div>
			<div class="control-group <?php echo form_error('posts')==''?'':'error';?>">
				<label class="control-label">主机地址</label>
				<div class="controls">
					<input type="text" name="h_address" value="<?php echo set_value('h_address'); ?>"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">后台用户名</label>
				<div class="controls">
					<input type="text" name="b_name" value="<?php echo set_value('b_name'); ?>"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">后台密码</label>
				<div class="controls">
					<input type="text" name="b_password" value="<?php echo set_value('b_password'); ?>"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">FTP用户名</label>
				<div class="controls">
					<input type="text" name="ftp_name" value="<?php echo set_value('ftp_name'); ?>"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">FTP密码</label>
				<div class="controls">
					<input type="text" name="ftp_password" value="<?php echo set_value('ftp_password'); ?>"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">后台类型</label>
				<div class="controls">
					<?php echo form_dropdown('b_type',$typeoptions)?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">状态</label>
				<div class="controls">
					<?php echo form_dropdown('status',$statusoptions)?>
					<?=form_error('status') ?>
				</div>
			</div>
		
			<div class="control-group">
				<label class="control-label">备注</label>
				<div class="controls">
					<textarea name="remark"><?php echo set_value('remark'); ?></textarea>
				</div>
			</div>
			<div class="control-group form-actions">
			    <button type="button" onclick="history.back()" class="btn">返回</button>
				<button type="reset" class="btn">重置</button>
				<button type="submit" name="submit" class="btn btn-primary">添加</button>
			</div>
		</form>
	</div>
	<!-- insert table end -->
				
</div>	
<?php $this->load->view('inc/foot'); ?>