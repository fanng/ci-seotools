<?php $this->load->view('inc/head'); ?>
<?php 
$tmpl = array (
		'table_open' => '<table class="table table-striped table-condensed">',
);

$this->table->set_heading("编号","名称","负责人")
?>
<div class="container">
	
    <div class="row">
		<section class="span5">
			<h3>项目组</h3>
			<?php 
			$this->table->set_template ( $tmpl );
			$query = $this->db->query("SELECT * FROM hospital_project");
            echo $this->table->generate($query);
            ?>
		</section> 
	</div>
<?php $this->load->view('inc/foot'); ?>