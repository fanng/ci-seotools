<?php $this->load->view('inc/head'); ?>
<script>
$(function(){
	$(":checkbox").click(function(){
		var is_order = '0';
		if($(this).attr("checked")=='checked') is_order = '1';
		
				var dataString = "when="+$(this).val()+"&is_order="+is_order;
				$.ajax({
                    type: "POST",
                    //contentType: "application/json",
                    url: "/index.php/order/orderChange",
                    data: dataString,
                    dataType: 'json',
                    success: function(data, status) {
                        $('#cha'+data.id).html(data.searchdate);
                    }
                })
	     }
	)
})
</script>
<div class="row-fluid">
	<div class="container-fluid">
		<header class="jumbotron subhead" id="overview">
			<legend>域名管理</legend>
		</header>
		<div class="row-fluid show-grid" id="toolbar">
			<!-- LEFT BUTTON START -->
			<div class="span4">
				<div class="left">
					<button type="button" href="javascript:void(0)"
						onclick="SelectAll()" class="btn btn-small">全选</button>
					<button class="btn btn-small" type="button"
						onclick="location='/index.php/website/domainAdd'">添加</button>
					<button type="button" href="javascript:void(0)" onclick="delItem()"
						class="btn btn-small btn-danger">删除</button>
				</div>
			</div>
			<!-- LEFT BUTTON END -->
		</div>
<?php

$statusoptions = array('在线','离线','维护');

$typeoptions=array();
$projectoptions=array();
foreach (@$type as $r =>$p)
{
    $typeoptions[$p->id] = $p->name;
}

foreach(@$project as $r =>$p)
{
	$projectoptions[$p->id] = $p->name;
}

$tmpl = array (
		'table_open' => '<table class="table table-condensed table-striped">',
);
$this->table->set_heading('选','所属项目','域名','注册时间','过期时间','状态','操作');

//table列表
foreach (@$site as $r => $d)
{
	$this->table->add_row ( '<input type="checkbox">', @$projectoptions[$d->pid], '<a href="' . $d->domain . '" target="_blank">' . $d->domain . '</a>',  $d->btime, $d->etime, $this->Website_model->status_View(@$statusoptions[$d->status]), '<a href="/index.php/website/update?id='.$d->id.'">修改'
                      );
}

$this->table->set_template ( $tmpl );
echo $this->table->generate();

?>
</div>
<?php $this->load->view('inc/foot');?>	