<?php $this->load->view('inc/head'); ?>
<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>注册用户</legend>
	</header>
<div class="well">
<?php
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
$this->form_validation->set_error_delimiters('<span class="help-inline">','</span>');
$formattributes = array('class' => 'form-horizontal');
$label = array('class' => 'control-label');

?>
<?=form_open($this->uri->uri_string(),$formattributes) ?>
	<?php if ($use_username) { ?>
	<div class="control-group  <?php echo form_error('username')==''?'':'error';?>">
		<label for="username" class="control-label">用户名</label>
		<div class="controls">
		<input type="text" name="username" value="" id="username"  />
		    <?=form_error('username'); ?>
		    <?php echo isset($errors['username'])?$errors['username']:''; ?>
		</div>
	</div>
	<?php } ?>
	<div class="control-group  <?php echo form_error('email')==''?'':'error';?>">
		<label for="email" class="control-label">邮箱地址</label>
		<div class="controls">
		<input type="text" name="email" value="" id="email" />
		<?=form_error('email'); ?>
		<?php echo isset($errors['email'])?$errors['email']:''; ?>
		</div>
	</div>
	<div class="control-group  <?php echo form_error('password')==''?'':'error';?>">
		<label for="password" class="control-label">密码</label>
		<div class="controls">
		<input type="password" name="password" value="" id="password" />
		<?=form_error('password') ?>
		</div>
	</div>
	<div class="control-group  <?php echo form_error('confirm_password')==''?'':'error';?>">
		<label for="confirm_password" class="control-label">重复密码</label>
		<div class="controls">
		    <input type="password" name="confirm_password" value="" id="confirm_password" />
		    <?=form_error('confirm_password') ?>
		</div>
	</div>

	<?php if ($captcha_registration) {
		if ($use_recaptcha) { ?>
	<tr>
		<td colspan="2">
			<div id="recaptcha_image"></div>
		</td>
		<td>
			<a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
			<div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
			<div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
		</td>
	</div>
	<tr>
		<td>
			<div class="recaptcha_only_if_image">Enter the words above</div>
			<div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
		</td>
		<td><input type="text" id="recaptcha_response_field" name="recaptcha_response_field" /></td>
		<td style="color: red;"><?=form_error('recaptcha_response_field') ?></td>
		<?=$recaptcha_html ?>
	</div>
	<?php } else { ?>
	<div class="control-group">
			<label class="control-label">输入验证码</label>
			<div class="controls"><?=$captcha_html ?></div>
		</td>
	</div>
	<div class="control-group  <?php echo form_error('captcha')==''?'':'error';?>">
		<label for="captcha" class="control-label">验证码</label>
		<div class="controls">
		    <input type="text" name="captcha" value="" id="captcha" maxlength="8"  />
		    <?=form_error('captcha') ?>
		</div>
	</div>
	<?php }
	} ?>
	<div class="form-actions">
       <button type="submit" class="btn btn-primary" name="register"  >注册</button>
    </div>
<?=form_close() ?>
</div>
</div>
<?php $this->load->view('inc/foot'); ?>