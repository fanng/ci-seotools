<?php $this->load->view('inc/head'); ?>
<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>登陆</legend>
	</header>
<div class="well">
<?php

if ($login_by_username AND $login_by_email) {
	$login_label = '邮箱/登录名';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}

$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);

$this->form_validation->set_error_delimiters('<span class="help-inline">','</span>');

$formattributes = array('class' => 'form-horizontal');
$label = array('class' => 'control-label');
?>
<?=form_open('c=auth&m=login',$formattributes) ?>
    <div class="control-group  <?php echo form_error('login')==''?'':'error';?>">
		<?php echo form_label($login_label, 'login',$label); ?>
		<div class="controls">
		<input type="text" name="login" value="" id="login" />
			<?=form_error('login') ?>
			<?php echo isset($errors['login'])?$errors['login']:''; ?>
		</div>
	</div>
	<div class="control-group <?php echo form_error('password')==''?'':'error';?>">
		<label for="password" class="control-label">密码</label>
		    <div class="controls">
		        <input type="password" name="password" value="" id="password" />
			<?=form_error('password') ?>
			<?php echo isset($errors['password'])?$errors['password']:''; ?>
		</div>
	</div>
	<?php if ($show_captcha) {
		if ($use_recaptcha) { ?>
	<div class="control-group">
		<td colspan="2">
			<div id="recaptcha_image"></div>
		</td>
		<td>
			<a href="javascript:Recaptcha.reload()">刷新验证码</a>
			<div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">获取音频验证码</a></div>
			<div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">获取图片验证码</a></div>
		</td>
	</div>
	<div class="control-group">
		<td>
			<div class="recaptcha_only_if_image">输入上面的字符</div>
			<div class="recaptcha_only_if_audio">输入您听到的字符</div>
		</td>
		<td><input type="text" id="recaptcha_response_field" name="recaptcha_response_field" /></td>
		<td style="color: red;"><?=form_error('recaptcha_response_field') ?></td>
		<?=$recaptcha_html ?>
	</div>
	<?php } else { ?>
	<div class="control-group">
			<label class="control-label">输入验证码</label>
			<div class="controls">
			    <?php echo $captcha_html; ?>
			</div>
	</div>
	<div class="control-group <?php echo form_error($captcha['name'])==''?'':'error';?>">
		<?=form_label('验证码', $captcha['id'],$label) ?>
		<div class="controls"><?=form_input($captcha) ?>
		    <?=form_error($captcha['name']) ?>
		</div>
	</div>
	<?php }
	} ?>
	<div class="control-group">
			<label for="remember" class="control-label">记住我</label>
			<div class="controls"><input type="checkbox" name="remember" value="1" id="remember"  /></div>	
			
	</div>
	<div class="form-actions">
	     <input type="submit" name="submit" class="btn btn-primary" value="让我登陆"  />
	     <button class="btn"><?php echo anchor('/auth/forgot_password/', '忘记密码'); ?></button>
		 <button class="btn"><?php if ($this->config->item('allow_registration', 'fx_auth')) echo anchor('/auth/register/', '注册'); ?></button>
    </div>     
<?=form_close() ?>
</div>
</div>
<?php $this->load->view('inc/foot'); ?>