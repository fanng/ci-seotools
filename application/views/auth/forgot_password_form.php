<?php $this->load->view('inc/head'); ?>
<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>忘记密码</legend>
	</header>
<div class="well">
<?php
if ($this->config->item('use_username', 'fx_auth')) {
	$login_label = 'Email or login';
} else {
	$login_label = 'Email';
}
$formattributes = array('class' => 'form-horizontal');
$label = array('class' => 'control-label');

?>
<?php echo form_open($this->uri->uri_string(),$formattributes); ?>
    <div class="control-group <?php echo form_error('login')==''?'':'error';?>"> 
		<label for="login" class="control-label">邮箱/登陆名</label>
		<div class="controls"><input type="text" name="login" value="" id="login" />
		        <?php echo form_error('login','<span class="help-inline">','</span>'); ?>
		        <?php echo isset($errors['login'])?$errors['login']:''; ?>
		    </span>
		</div>
	</div>
<div class="form-actions">
<input type="submit" name="reset" class="btn btn-primary" value="获取新密码"  />
<button type="button" onclick="location.href='/index.php/auth/login/'" class="btn">登陆</button>
</div>
<?php echo form_close(); ?>
</div>
</div>
<?php $this->load->view('inc/foot'); ?>