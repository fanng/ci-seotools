<?php @$base_url=$this->config->item('base_url')?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo @$config['title'] ;?> - <?php echo $this->config->item('site_title'); ?></title>
<link rel="stylesheet" href="<?=$base_url?>/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="<?=$base_url?>/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="<?=$base_url?>/css/main.css" />
<link rel="stylesheet" href="<?=$base_url?>/css/bootstrap-datetimepicker.min.css" />
<script src="http://lib.sinaapp.com/js/jquery/1.7.2/jquery.min.js"></script>
<script src="<?=$base_url?>/js/jquery.form.js"></script>
<script src="<?=$base_url?>/js/bootstrap.min.js"></script>
<script src="<?=$base_url?>/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?=$base_url?>/js/main.js"></script>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse"
					data-target=".nav-collapse"> <span class="icon-bar"></span> <span
					class="icon-bar"></span> <span class="icon-bar"></span>
				</a> <a class="brand" href="<?=$base_url?>"><?php echo $this->config->item('site_title'); ?></a>
				<div class="nav-collapse">
					<ul class="nav">
					    <?php if($this->fx_auth->is_logged_in()):?>
						<li <?php if ($this->uri->segment(1) == "summary") echo ' class="active"'; ?>><a href="/index.php/summary/">摘要</a></li>
						<li <?php if ($this->uri->segment(1) == "rank_keyword") echo ' class="active"'; ?>><a href="<?=$base_url?>/index.php/rank_keyword">占位查询</a></li>
                            <li <?php if ($this->uri->segment(1) == "urlzhanwei") echo ' class="active"'; ?>><a href="<?=$base_url?>/index.php/urlzhanwei">URL占位查询</a></li>
						<li <?php if ($this->uri->segment(1) == "url_keyword") echo ' class="active"'; ?>><a href="<?=$base_url?>/index.php/url_keyword">排名查询</a></li>
						<!--<li <?php if ($this->uri->segment(1) == "collection") echo ' class="active"'; ?>><a href="<?=$base_url?>/index.php/collection">网址采集</a></li>-->
						<li <?php if ($this->uri->segment(1) == "hospital") echo ' class="active"'; ?>><a href="<?=$base_url?>/index.php/hospital/unit">单元管理</a></li>
						<li <?php if ($this->uri->segment(1) == "count") echo ' class="active"'; ?>><a href="/index.php/count/index/">绩效统计</a></li>
						<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="">用户<b class="caret"></b></a>
						    <ul class="dropdown-menu">
								<li><a href="/index.php/backend/users/">用户管理</a></li>
								<li><a href="/index.php/backend/roles/">用户组/角色</a></li>
								<li><a href="/index.php/backend/custom_permissions/">自定义权限</a></li>
								<li><a href="/index.php/backend/uri_permissions/">管理URL权限</a></li>
							</ul>
						</li>
                        <!--
						<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="">公司相关<b class="caret"></b></a>
						    <ul class="dropdown-menu">
						        <li><a href="/index.php/company/personnel">公司员工</a></li>
								<li><a href="/index.php/order/">订餐</a></li>
							</ul>
						</li>

						<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="">网站相关<b class="caret"></b></a>
						    <ul class="dropdown-menu">
						        <li><a href="/index.php/website/backstage_ftp">后台/FTP</a></li>
						        <li><a href="/index.php/website/domain">域名管理</a></li>
						        <li><a href="/index.php/website/status">网站状态</a></li>
						        <li><a href="/index.php/website/basic">基本信息配置</a></li>
							</ul>
						</li>
						-->
						<?php endif?>
					</ul>

					 <?php if($this->fx_auth->is_logged_in()):?>
					<ul class="nav pull-right">
						<li class="divider-vertical"></li>
						<li class="dropdown">
						<a href="#" class="dropdown-toggle"
							data-toggle="dropdown"><?php echo $this->fx_auth->get_username()?> <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="/index.php/auth/logout/ ">退出</a></li>
							</ul></li>
					</ul>
					<?php endif?>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">