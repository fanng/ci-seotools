<?php $this->load->view('inc/head'); ?>

<div class="row-fluid">
	<header class="jumbotron subhead" id="overview" style="padding-bottom:20px">
		<!-- <div class="page-header">
		</div> -->
		
	    
	    <legend>关键词导入</legend>
		
        <!--
		<div class="">
			<ul class="nav nav-pills">
				<li><a
					href="<?php @$base_url=$this->config->item('base_url')?>/index.php/rank_keyword">首页
				</a></li>
				<li class="active"><a
					href="<?php @$base_url=$this->config->item('base_url')?>/index.php/rank_keyword/insertView">关键词导入
				</a></li>
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">功能 <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#navs">功能 </a></li>
						<li><a href="#navbar">功能 </a></li>
						<li><a href="#breadcrumbs">功能 </a></li>
						<li><a href="#pagination">功能 </a></li>
					</ul></li>
			</ul>
		</div>-->
	</header>

	<!-- insert table -->
	<div class="">
		<form class="form-horizontal"
			action="<?php @$base_url=$this->config->item('base_url')?>/index.php/url_keyword/insert"
			name="iform" method="POST">
			<div class="control-group">
				<label class="control-label" for="select01">单元</label>
				<div class="controls">
					<select id="select01" name="unit">
						<?php foreach($hospital_unit as $v =>$hospital_unit):?>
						<?php echo "<option value=".$hospital_unit['id'].">".$hospital_unit['h_title']."</option>";?>
						<?php endforeach;?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="select01">搜索引擎</label>
				<div class="controls">
					<select id="select01" name="engine">
						<option>百度</option>
						<option>谷歌</option>
						<option>360搜索</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="select01">链接URL</br>(一行一条)</label>
				<div class="controls">
					<textarea class="span6" name="url" id="url" rows="15"></textarea>
				</div>
			</div>
			<input type="hidden" name="uid" id="uid" value="1" /> <input
				type="hidden" name="author" value="fangyang" />
			<div class="control-group form-actions">
				<button type="reset" class="btn">重置</button>
				<button type="submit" class="btn btn-primary">仅仅导入</button>
			</div>
		</form>
	</div>
	<!-- insert table end -->

</div>

<?php $this->load->view('inc/foot'); ?>