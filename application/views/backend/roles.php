<?php $this->load->view('inc/head'); ?>
<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>用户角色</legend>
	</header>
	<?php
	$this->load->library ( 'table' );
	
	// Show error
	echo validation_errors ();
	
	// Build drop down menu
	$options [0] = 'None';
	foreach ( $roles as $role )
	{
		$options [$role->id] = $role->name;
	}
	
	// Build table
	// $this->table->set_heading('', 'ID', 'Name', 'Parent ID');
	
	{
		// $this->table->add_row(form_checkbox('checkbox_'.$role->id,
	// $role->id), $role->id, $role->name, $role->parent_id);
	}
	
	$formattributes = array('class' => 'form-horizontal');
	$label = array('class' => 'control-label');
	
	// Build form
	echo form_open ( $this->uri->uri_string (),$formattributes );
	
	echo '<div class="control-group"> ';
	echo form_label ( '用户组', 'role_parent_label' ,$label);
	echo '<div class="controls">'.form_dropdown ( 'role_parent', $options ).'</div>';
	echo '</div>';
	
	echo '<div class="control-group"> ';
	echo form_label ( '组名', 'role_name_label' ,$label);
	echo '<div class="controls">'.form_input ( 'role_name', '' ).'</div>';
	echo '</div>';
	
	echo '<div class="form-actions"> ';
	echo form_submit ( 'add', '添加权限' ,'class="btn"');
	echo '&nbsp;&nbsp;';
	echo form_submit ( 'delete', '删除所选权限' ,'class="btn btn-danger"');
	echo '</div>';
	
	echo '<hr/>';
	
	?>
	<table cellpadding="5" class="table table-striped table-bordered table-condensed">
	    <thead>
		<tr>
			<th>选;</th>
			<th>编号</th>
			<th>用户名</th>
			<th>组编号</th>
		</tr>
		</thead>
		<?php foreach ($roles as $role) { ?>
		<tr>
			<td><input type="checkbox" name="checkbox_<?=$role->id?>"
				value="<?=$role->id?>" /></td>
			<td><?=$role->id?></td>
			<td><?=$role->name?></td>
			<td><?=$role->parent_id?></td>
		</tr>
		<?php } ?>
	</table>
	<?php
	
	// Show table
	// echo $this->table->generate();
	
	echo form_close ();
	
	?>
</div>
</div>
<?php $this->load->view('inc/foot'); ?>