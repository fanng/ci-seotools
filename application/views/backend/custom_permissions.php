<?php $this->load->view('inc/head'); ?>
<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>管理自定义权限</legend>
	</header>
	<?php
		echo '<b>管理你想更改的用户组权限</b><br/><br/>';
		
		// Build drop down menu
		foreach ($roles as $role)
		{
			$options[$role->id] = $role->name;
		}
        
		// Change allowed uri to string to be inserted in text area
		if ( ! empty($allowed_uri))
		{
			$allowed_uri = implode("\n", $allowed_uri);
		}
		
		if (empty($edit))
		{
			$edit = FALSE;
		}
			
		if (empty($delete))
		{
			$delete = FALSE;
		}
		
		$formattributes = array('class' => 'form-horizontal');
		$label = array('class' => 'control-label');
		
		// Build form
		echo form_open($this->uri->uri_string(),$formattributes);
		
		echo '<div class="control-group"> ';
		echo form_label('角色', 'role_name_label',$label);
		echo '<div class="controls">'.form_dropdown('role', $options); 
		echo form_submit('show', '显示权限','class="btn"').'</div>'; 
		echo form_label('', 'uri_label');
		echo '</div> ';
		
		
		echo '<div class="control-group"> ';
		//echo form_label('', 'uri_label',$label);
				
		echo form_label('允许编辑', 'edit_label',$label);
		echo '<div class="controls">'.form_checkbox('edit', '1', $edit).'</div>';
		echo '</div> ';
		
		echo '<div class="control-group"> ';
		echo form_label('允许删除', 'delete_label',$label);
		echo '<div class="controls">'.form_checkbox('delete', '1', $delete).'</div>';
		echo '</div> ';
		
		echo '<div class="form-actions"> ';
		echo form_submit('save', '保存权限','class="btn btn-primary"');
		echo '</div> ';
		
		echo '<div class="alert alert-info">';
		echo '用刚修改的用户登陆,通过点击 '.anchor('backend/custom_permissions/').' 来查看结果<br/>';
		echo '如果你更改的是自己的权限,你需要重新登陆来查看更改结果';
		echo '</div>';
		
		echo form_close();
			
	?>
<?php $this->load->view('inc/foot'); ?>