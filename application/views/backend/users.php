<?php $this->load->view('inc/head'); ?>
<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>管理用户</legend>
	</header>
	<?php
	// Show reset password message if exist
	if (isset ( $reset_message ))
		echo $reset_message;
		
		// Show error
	echo validation_errors ();
	
	$btnclass = array (
			'class' => 'btn' 
	);
	
	$tmpl = array (
			'table_open' => '<table class="table table-striped table-bordered table-condensed">',
	);
	$this->table->set_template ($tmpl );
	
	$this->table->set_heading ( '', '用户名', '邮箱地址', '权限', '是否激活', '最后登录IP', '最后登录时间', '创建时间' );
	
	foreach ( $users as $user )
	{
		$banned = ($user->banned == 1) ? 'Yes' : 'No';
		
		$this->table->add_row ( form_checkbox ( 'checkbox_' . $user->id, $user->id ), $user->username, $user->email, $user->role_name, $banned, $user->last_ip, date ( 'Y-m-d', strtotime ( $user->last_login ) ), date ( 'Y-m-d', strtotime ( $user->created ) ) );
	}
	
	echo form_open ( $this->uri->uri_string () );
	
	echo form_submit ( 'ban', '禁止用户', 'class="btn"' );
	echo form_submit ( 'unban', '激活用户', 'class="btn"' );
	echo form_submit ( 'reset_pass', '重置密码', 'class="btn"' );
	
	echo '<hr/>';
	
	echo $this->table->generate ();
	
	echo form_close ();
	
	echo $pagination;
	
	?>
<?php $this->load->view('inc/foot'); ?>