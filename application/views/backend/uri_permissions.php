<?php $this->load->view('inc/head'); ?>
<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>管理URL访问权限</legend>
	</header>
	<?php
	// Build drop down menu
	foreach ( $roles as $role )
	{
		$options [$role->id] = $role->name;
	}
	
	// Change allowed uri to string to be inserted in text area
	if (! empty ( $allowed_uris ))
	{
		$allowed_uris = implode ( "\n", $allowed_uris );
	}
	
	$formattributes = array('class' => 'form-horizontal');
	$label = array('class' => 'control-label');
	$btnclass = 'class="btn"';
	// Build form
	echo form_open ( $this->uri->uri_string () ,$formattributes);
	
	echo '<div class="control-group"> ';
	echo form_label ( '角色', 'role_name_label' ,$label);
	echo '<div class="controls">'.form_dropdown ( 'role', $options );
	echo form_submit ( 'show', '显示用户可以访问的URL',$btnclass ).'</div>';
	
	echo form_label ( '', 'uri_label' );
	
	echo '</div> ';
	
	echo '<div class="control-group"> ';
	echo '<label class="control-label">';
	echo '允许的URL(一行一个) :<br/>';
	echo '</label>';
	echo '<div class="controls">'.form_textarea ( 'allowed_uris', $allowed_uris ,'class="span4"' ).'</div>';
	echo '</div> ';
	
	echo '<div class="form-actions"> ';
	echo form_submit ( 'save', '保存URL权限' ,$btnclass);
	echo '</div> ';
	
	echo '<div class="alert alert-info">';
	echo '<h4 class="alert-heading">使用规则!</h4>';
	echo "<p>输入'/'允许角色访问所有的URI.</p>";
	echo "<p>输入'/controller/'允许角色访问控制器和它的功能</p>";
	echo "<p>输入'/controller/function/'允许角色访问控制器里的功能</p>";
	echo '<p>这些权限设置只有在控制器里使用check_uri_permissions()后才能使用</p>';
	echo '</div>';
	
	echo form_close ();
	?>
</div>
</div>
<?php $this->load->view('inc/foot'); ?>