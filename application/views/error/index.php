<?php $this->load->view('inc/head'); ?>

<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>错误的页面</legend>
	</header>
	<div class="well">
		<ul>
			<li>你所访问的ip不被允许</li>
			<li>如果你使用的是VPN，建议您先暂时关闭</li>
		</ul>
	</div>
<?php $this->load->view('inc/foot');?>