<?php $this->load->view('inc/head'); ?>
<script>
$(function(){
	$(":checkbox").click(function(){
		var is_order = '0';
		if($(this).attr("checked")=='checked') is_order = '1';
		
				var dataString = "when="+$(this).val()+"&is_order="+is_order;
				$.ajax({
                    type: "POST",
                    //contentType: "application/json",
                    url: "/index.php/order/orderChange",
                    data: dataString,
                    dataType: 'json',
                    success: function(data, status) {
                        $('#cha'+data.id).html(data.searchdate);
                    }
                })
	     }
	)
})
</script>
<div class="container">
<div class="row-fluid">
	<header class="jumbotron subhead" id="overview">
		<legend>订餐</legend>
	</header>
<?php

//截止时间
$disabled = '';
$endate = date('Y-m-d 14:00:00');
if(strtotime($endate)<strtotime(date('Y-m-d h:m:s')))
{
	$disabled = 'disabled';
}
$weekarray=array("日","一","二","三","四","五","六");
$today = date('Y-m-d');
$tomorrow = date("Y-m-d", mktime(0,0,0,date("m"),date("d")+1,date("Y")));
$acquired = date("Y-m-d", mktime(0,0,0,date("m"),date("d")+2,date("Y")));
//用于交互的时间
$today1 = date('Y-m-d 6:00:00');
$today2 = date('Y-m-d 18:00:00');
$tomorrow1 = date("Y-m-d 6:00:00", mktime(0,0,0,date("m"),date("d")+1,date("Y")));
$tomorrow2 = date("Y-m-d 18:00:00", mktime(0,0,0,date("m"),date("d")+1,date("Y")));
$acquired1 = date("Y-m-d 6:00:00", mktime(0,0,0,date("m"),date("d")+2,date("Y")));
$acquired2 = date("Y-m-d 18:00:00", mktime(0,0,0,date("m"),date("d")+2,date("Y")));
$week1 = $weekarray[date("w", mktime(0,0,0,date("m"),date("d")+1,date("Y")))];
$week2 = $weekarray[date("w", mktime(0,0,0,date("m"),date("d")+2,date("Y")))];

$tmpl = array (
		'table_open' => '<table class="table table-condensed table-striped">',
);
$this->table->set_heading('编号', '姓名','今天(上)','今天(下)', '明天(上) / '.$tomorrow.' / '.$week1,'明天(下) / '.$tomorrow.' / '.$week1,'后天(上) / '.$acquired.' / '.$week2,'后天(下) / '.$acquired.' / '.$week2);

foreach($users as $r =>$user)
{
	$this->table->add_row($user->id, 
                          $user->name, 
                          '<input type="checkbox" value="tomorrow1/'.$user->id.'" '.$this->Order_model->getOrder($user->id,$today1).' disabled>',
                          '<input type="checkbox" value="tomorrow2/'.$user->id.'" '.$this->Order_model->getOrder($user->id,$today2).' disabled>',
                          '<input type="checkbox" value="tomorrow1/'.$user->id.'" '.$this->Order_model->getOrder($user->id,$tomorrow1).' '.$disabled.'>',
                          '<input type="checkbox" value="tomorrow2/'.$user->id.'" '.$this->Order_model->getOrder($user->id,$tomorrow2).' '.$disabled.'>',
                          '<input type="checkbox" value="acquired1/'.$user->id.'" '.$this->Order_model->getOrder($user->id,$acquired1).'>',
                          '<input type="checkbox" value="acquired2/'.$user->id.'" '.$this->Order_model->getOrder($user->id,$acquired2).'>'
                         );
}

$this->table->set_template ( $tmpl );
echo $this->table->generate();

?>
<!-- 统计结果 start-->
<legend>统计结果</legend>

<?php 

$this->table->set_heading('总人数', '今天(上) / '.$today, '今天(下) / '.$today,'明天(上) /'.$tomorrow,'明天(下) /'.$tomorrow,'后天(上) / '.$acquired,'后天(下) / '.$acquired);

$this->table->add_row($this->Company_model->peopleNum(),
                      $this->Order_model->getOrderNum($today1),
                      $this->Order_model->getOrderNum($today2),
                      $this->Order_model->getOrderNum($tomorrow1),
                      $this->Order_model->getOrderNum($tomorrow2),
                      $this->Order_model->getOrderNum($acquired1),
                      $this->Order_model->getOrderNum($acquired2)
                     );

$this->table->set_template ( $tmpl );
echo $this->table->generate();
?>
<!-- 统计结果 end -->
</div>
<?php $this->load->view('inc/foot');?>	