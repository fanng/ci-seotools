<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         网址采集MODEL
 * @author        fangyang (funyung@163.com)
 * @since         2013-4-7 11:03:50
 */
header("Content-Type:text/html;charset=utf-8");
include  dirname(__FILE__).'../../controllers/Snoopy.class.php';

class Collection_model extends CI_Model 
{
	/**
	 * 构造函数
	 */
	
	public function __construct()
	{
		parent::__construct();
	}
    
    /**
     * 针对不同网页编码进行转码
     * @return string
     */
    public function my_encoding($data, $to)
    {
        $encode_arr = array(
                'UTF-8',
                'ASCII',
                'GBK',
                'GB2312',
                'BIG5',
                'JIS',
                'eucjp-win',
                'sjis-win',
                'EUC-JP'
        );
        $encoded = mb_detect_encoding($data, $encode_arr);
        $data = mb_convert_encoding($data, $to, $encoded);
        return $data;
    }
    
    /**
     * 关键词的竞争力
     * @param $num_spread
     * @return string
     */
    public function key_weight($num_spread)
    { 
        /**
         * 计算公式 (上/8+下/3+右/8)/3
         */
        //$num_spread =array();
        $num_spread = explode(',',$num_spread);
        if(is_array($num_spread)||(count($num_spread)==3))
        {
            $key_formula = number_format((($num_spread[0] / 8 + $num_spread[1] / 3 +  $num_spread[2] / 8) / 3),2)*100;
            return $key_formula;
        }else {
            return '';
        }
        
    }
    
    /**
     * 百度占位查询  //如果传入的是数组，一样可以查询
     * @param array/string $keyword
     * @param $unit
     * @param $id
     * @return void
     */
	function search_baidu ($keyword,$page = 1)
	{
	    static $px = 0,$contents,$urlArr = array();
	    $snoopy = new Snoopy();
	    
	    $enKeyword = urlencode($keyword);
	    
	    for($i=1;$i<=$page;$i++)
	    {
	    	$firstRow = ($i - 1) * 10;
	    	$snoopy->fetch("http://www.baidu.com/s?wd=$enKeyword&pn=$firstRow");
	    	$scontent = $snoopy->results;
	    }
	    
	    
	    preg_match_all ( '/<table[^>]*?class="result"[^>]*>[\s\S]*?<\/table>/i', $scontent, $rs );
	    
	    foreach ( $rs [0] as $k => $v )
		{
			$px ++;
			
			// $rsState = true;
			preg_match_all ( '/<h3[\s\S]*?(<a[\s\S]*?<\/a>)/', $v, $rs_t );
			preg_match_all ( '/<span class="g">*?(<a[\s\S]*?<\/a>)/', $v, $rs_u );
			// echo '当前 "' . $url . '" 在百度关键字 "' . $keyword . '" 中的排名为：'
			// . $px;
			// echo '第' . $page . '页;第' . ++ $k . "个<a target='_blank'
			// href='http://www.baidu.com/s?wd=$enKeyword&&pn=$firstRow'>进入百度</a>";
			// echo $rs_t[1][0];
			
			
			
			
			$rarray = $snoopy->fetchtext ( $v );
			print_r($rarray);
			//@$urlArray = array_merge ( $urlArray, $rarray );
			
		}
	    /*
	    foreach($urlArr as $a)
	    {
	    	if(strstr($a,'com'))
	    	{
	    		echo $a.'<br/>';
	    	}
	    }
	    */
	    //echo $a;;
	    
	    //preg_match_all('/<table[^>]*?class="result"[^>]*>[\s\S]*?<\/table>/i', $contents, $rs);
	    
	    //unset($contents);
	   
	}
    
    
    
    /**
     * 查询搜索结果，用于前台ajax返回值
     * @param unknown_type $id
     * @param $db
     * @return array $data
     */
    public function rankResult($id,$db)
    {
		static $data;
		$this->db->select ( '*' )->from ( $db )->where ( 'id', $id );
		$q = $this->db->get ();
		if ($q->num_rows () > 0)
		{
			foreach ( $q->result_array () as $row )
			{
				$data [] = $row;
			}
		}
		return $data;
	}
	
	
	/**
	 * url收录查询
	 * @param $unit
	 * @param $id
	 * @return void
	 */
	/*
	public function url_search ($id,$url)
	{
		static $end_time=array(),$pageinfo = array ();
		$pageinfo ['is_record'] = 1;
		$pageinfo ['title'] = '';
		$snoopy = new Snoopy ();
		
		// 是否收录
		$snoopy->fetch ( 'http://www.baidu.com/s?wd=' . $url );
		$contents = $snoopy->results;
		if (strstr ( $contents, '抱歉，没有找到与' ))
		{
			$pageinfo ['is_record'] = 0;
		}
		// 快照日期
		$tt = "/(\d{4}\-\d{1,2}-\d{1,2})(.*?)<\/span>/i";
	    //preg_match ( $tt, $contents, $end_time );
		if (preg_match ( $tt, $contents, $end_time ))
		{
			$pageinfo ['snapshot'] = $end_time [1];
		} else
		{
			$pageinfo ['snapshot'] = '';
		}
		// 标题
		unset ( $contents );
		$snoopy->fetch ( $url );
		$contents = $snoopy->results;
		preg_match ( "/<title>(.*)<\/title>/smUi", $contents, $matches );
		$pageinfo ['title'] = trim ( $matches [1] );
		$pageinfo ['title']=$this->my_encoding ( $pageinfo ['title'], 'UTF-8' );
		
		$pageinfo['searchdate'] = date ( 'Y-m-d H:s:m' );
		
		$this->saveResult($id,$pageinfo,'keyword_url');
	}
	*/
    
}


