<?php

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
/**
 * @title
 *
 * @author fangyang (funyung@163.com)
 * @since 2013-4-9 7:04:22
 *       
 */
class Hospital_model extends CI_Model
{
	private $Hospital_project_table = 'hospital_project';
	private $Hospital_table = 'hospital_unit';
	private $Hospital_project = 'hospital_project';
	/**
	 * 构造函数
	 */
	public function __construct()
	{
		parent::__construct ();
		$this->load->database();
	}
	
	/**
	 * 获取单元
	 * 
	 * @return array
	 */
	public function hospital_unit()
	{
		$table = $this->Hospital_table;
		$this->db->select ( '*' )->from ( $table );
		$q = $this->db->get ();
		return $q->result();
	}
	
	
	/**
	 * 获取所有项目组资料
	 * 
	 * @return array
	 */
	function getHospitalProject()
	{
		$q = $this->db->get ($this->Hospital_project_table);
		return $q->result();
	}
	
	/**
	 * 获取指定的单元信息
	 * 
	 * @param unknown_type $id
	 */
	function get_info_byid($id)
	{ 
		$this->db->where('id',$id);
		$q = $this->db->get ($this->Hospital_table);
		return $q;
	}
	
	/**
	 * 获取指定
	 * @param unknown_type $id
	 * @return string
	 */
	function get_project_byid($id)
	{
		$this->db->select('name')->from($this->Hospital_project)->where('id', $id);
		$q = $this->db->get ();
		$row = $q->row_array();
		return @$row['name'];
	}
	
	/**
	 *
	 * @param unknown_type $data
	 * @param unknown_type $dataUnit
	 */
	public function insertNew($data)
	{
		$this->db->insert($this->Hospital_table,$data);
	}
	
	/**
	 * 
	 * @param unknown_type $id
	 * @param unknown_type $data
	 */
    function updateNew($id,$data)
    {
    	$this->db->where('id', $id);
    	$this->db->update($this->Hospital_table, $data);
    }
    
    /**
     * 
     * @param unknown_type $url
     * @return boolean
     */
    function is_exit($url = '')
    {
    	$is_exit = FALSE;
    	$this->db->select('id')->from($this->Hospital_table)->where('siteurl', $url);
    	$q=$this->db->get();
    	$is_exit = empty($q->num_rows)?TRUE:FALSE;
    	return $is_exit;
    }
    
    /**
     * 
     * @param unknown_type $id
     * @return NULL
     */
    function delete_byid($id)
    {
    	if(empty($id))
    	{
    		return NULL;
    	}else 
    	{
    		$this->db->delete($this->Hospital_table, array('id' => $id));
    	}
    }
}
 