<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         
 * @author		    fangyang (funyung@163.com)
 * @since		    2013-5-2 10:01:42
 *     
 */

class Company_model extends CI_Model
{

	private $com_list_table = 'company_list';
	private $com_post_table = 'company_post';

	/**
	 * 构造函数
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 获取公司全部员工
	 * @param
	 * @return array
	 */
	function fetch_all()
	{
		$q = $this->db->get ($this->com_list_table);
		return $q->result ();
	}
	
	/**
	 * 获取全部职位
	 */
	function fetch_all_posts()
	{
		$q = $this->db->get ($this->com_post_table);
		return $q->result ();
	}
	
	/**
	 * 获取总人数/员工
	 *
	 * @return
	 */
	function peopleNum()
	{
		$q=$this->db->get($this->com_list_table);
		return $q->num_rows;
	}
	
	/**
	 * 获取指定员工信息
	 * 
	 * @param  $id
	 * @return array
	 */
	function getInfo($id)
	{
		$result = array();
		$this->db->select ( '*' )
		         ->from ( $this->com_list_table )
		         ->where('id',$id);
		
		$q = $this->db->get ();
		$result=$q->result();
		return $result;
	}
	
	/**
	 * 插入新的员工
	 * 
	 * @param array $data
	 * @return null
	 */
    function insertNew($data)
    {
    	$this->db->insert($this->com_list_table, $data);
    }
    
    /**
     * 读取职位
     * 
     * @param int $id
     * @return 
     */
    function selectPost($id)
    {
    	$this->db->select('post_name');
    	$this->db->where('id', $id); 
    	$q = $this->db->get($this->com_post_table);
    	
		if ($q->num_rows () == 1)
		{
			$row = $q->row ();
			return $row->post_name;
		}else {
			
			return '未知';
		}
    }
    
    /**
     * 检测姓名是否重复
     * 
     * @param $name
     * @return 
     */
    function verify_name($name)
    {
    	static $is_allow =FALSE;
    	$this->db->select ( '*' )
    	         ->from ( $this->com_list_table )
    	         ->where('name',$name);
    	         
    	$q = $this->db->get ();
    	
    	if($q->num_rows ==0)
    	{
    		$is_allow = TRUE;
    	}
    	
    	return $is_allow;
    }
    
    /**
     * 更新员工信息
     * 
     * @param int $id
     * @param array $date
     */
    function updateNew($id,$data)
    {
    	$this->db->where('id', $id);
    	$this->db->update($this->com_list_table, $data);
    }
}

?>