<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         
 * @author		    fangyang (funyung@163.com)
 * @since		    2013-5-6 10:01:42
 *     
 */


class Website_model extends CI_Model
{
	
	private $website_table = 'website_main';
	private $BackstageType_table = 'website_backstage_type';
	/**
	 * 构造函数
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * 获取所有网站
	 * 
	 * @param
	 * @return
	 */
	function getAllSite()
	{
		$q = $this->db->get($this->website_table);
		return $q->result();
	}
	
	
	/**
	 * 获取所有域名
	 * 
	 * @return array
	 */
	function getAllDomain()
	{
		$q = $this->db->get($this->website_table);
		return $q->result();
	}
	
	/**
	 * 获取所有后台类型
	 * 
	 * @param
	 * @return
	 */
	function getBackstageType()
	{
		$this->db->select ( '*' )
		         ->from ( $this->BackstageType_table );
		$q = $this->db->get ();
		return $q->result();
	}
	
	/**
	 * 获取指定网站信息
	 *
	 * @param  $id
	 * @return array
	 */
	function getInfo($id)
	{
		$result = array();
		$this->db->select ( '*' )
		         ->from ( $this->website_table)
		         ->where('id',$id);
	
		$q = $this->db->get ();
		$result=$q->result();
		return $result;
	}
	
	/**
	 * 验证是否存在重复
	 */
	function verify($url)
	{
		static $is_allow =FALSE;
		$this->db->select ( '*' )
		         ->from ( $this->website_table )
		         ->where('url',$url);
		
		$q = $this->db->get ();
		 
		if($q->num_rows ==0)
		{
			$is_allow = TRUE;
		}
		 
		return $is_allow;
	}
	
	/**
	 * 添加新的数据
	 * 
	 * @param unknown_type $data
	 */
	function insertNew($data)
	{
		$this->db->insert($this->website_table, $data);
	}
	
	/**
	 * 更新网站信息
	 * 
	 * @param unknown_type $id
	 * @param unknown_type $data
	 */
    function updateNew($id,$data)
    {
    	$this->db->where('id', $id);
    	$this->db->update($this->website_table, $data);
    }
    
    /**
     * 网站状态
     * 0：在线
     * 1：离线
     * 2：维护
     * 
     * @param unknown_type $id
     * @return string
     */
    function status_View($id = '0')
    {
    	$status_icon ='<i class="icon-time"></i>'; //未知状态
    	switch($id)
    	{
    		case 0:
    			$status_icon ='<i class="icon-ok"></i>'; 
    			break;
    		case 1:
    			$status_icon ='<i class="icon-remove"></i>';
    			break;
    		case 2:
    			$status_icon ='<i class="icon-repeat"></i>';
    			break;
    	}
    	return $status_icon;
    }
	
}