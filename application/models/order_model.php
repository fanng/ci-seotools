<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         
 * @author		    fangyang (funyung@163.com)
 * @since		    2013-5-2 10:01:42
 *     
 */

class Order_model extends CI_Model
{
	
	private $order_list = 'order_list';
	
	/**
	 * 构造函数
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	
	function getAllorder()
	{
	}
	
	/**
	 * 获取指定员工指定时间的订餐情况
	 * 
	 * @param int $id
	 * @param datetime $when
	 * @return
	 */
	function getOrder($id,$when)
	{
		$this->db->select ( 'is_order' )
		         ->from ( $this->order_list )
		         ->where('uid',$id)
		         ->where('ordertime',$when);
		         
		$q = $this->db->get ();
		if($q->num_rows ==1)
		{
			$result = $q ->result ();
			
			//return $result[0]->is_order;
			
			if($result[0]->is_order==1)
			{
				return 'checked';
			}
		}
		
	}
	
	/**
	 * 写入订单结果
	 * 
	 * @param array $data
	 */
	function orderChange($data)
	{
		//判断是否重复
		$this->db->select ( '*' )
		         ->from ( $this->order_list )
		         ->where('uid',$data['uid'])
		         ->where('ordertime',$data['ordertime']);
		
		$q = $this->db->get ();
		 
		if($q->num_rows !=0)
		{
			$this->db->where('uid',$data['uid']);
			$this->db->where('ordertime',$data['ordertime']);
			$this->db->update( $this->order_list, $data); 
			
		}else {
			
			$this->db->insert ( $this->order_list, $data );
		}
	}
	
	/**
	 * 获取指定时间内的订餐人数
	 * 
	 * @param datetime $when
	 * @return int
	 */
	function getOrderNum($when)
	{
		$this->db->select ( '*' )
		         ->from ( $this->order_list )
		         ->where('ordertime',$when)
		         ->where('is_order','1');
		$q = $this->db->get ();
		
		return $q->num_rows;
	}
}

?>