<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         
 * @author		    fangyang (funyung@163.com)
 * @since		    2013-4-23 2:01:42
 *     
 */
 
class Count_model extends CI_Model
{
   private $dataTable = 'keyword_url';
    /**
     * 构造函数
     */
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->table_name = 'keyword_url';
	}
	
	/**
	 * 获取员工绩效
	 * @param unknown_type $name
	 * @param unknown_type $when
	 * @return array
	 */
	
	function jixiao($name,$when)
	{
		$table = $this->dataTable;
		
		$this->db->where("DATE_FORMAT(insertdate,'%Y-%m-%d')=", $when);
		$query = $this->db->get($table);
		return $query;
		
	}
}
 