<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         占位查询、收录查询通用MODEL //采用snoopy类进行采集
 * @author        fangyang (funyung@163.com)
 * @since         2013-4-7 11:03:50
 */
set_time_limit(0);
//header("Content-Type:text/html;charset=utf-8");
include  dirname(__FILE__).'../../controllers/Snoopy.class.php';
ini_set ( "open_basedir", "" );
ini_set ( "safe_mode", "Off" );
class Rank_baidu_model extends CI_Model 
{
	/**
	 * 构造函数
	 */
    public function index()
    {
        parent::__construct();
        $this->table_name = 'keyword_rank';
    }
    
    /**
     * 针对不同网页编码进行转码
     * @return string
     */
    public function my_encoding($data, $to)
    {
        $encode_arr = array(
                'UTF-8',
                'ASCII',
                'GBK',
                'GB2312',
                'BIG5',
                'JIS',
                'eucjp-win',
                'sjis-win',
                'EUC-JP'
        );
        $encoded = mb_detect_encoding($data, $encode_arr);
        $data = mb_convert_encoding($data, $to, $encoded);
        return $data;
    }
    
    /**
     * 关键词的竞争力
     * @param $num_spread
     * @return string
     */
    public function key_weight($num_spread)
    { 
        /**
         * 计算公式 (上/8+下/3+右/8)/3
         */
        //$num_spread =array();
        $num_spread = explode(',',$num_spread);
        if(is_array($num_spread)||(count($num_spread)==3))
        {
            $key_formula = number_format((($num_spread[0] / 8 + $num_spread[1] / 3 +  $num_spread[2] / 8) / 3),2)*100;
            return $key_formula;
        }else {
            return '';
        }
        
    }
    
    /**
     * 百度占位查询  //如果传入的是数组，一样可以查询
     * @param array/string $keyword
     * @param $unit
     * @param $id
     * @return void
     */
    public function search ($keyword, $unit, $id, $page = 1)
    {
    	$idArray = explode('|',$id);
    	//$id = $idArray;
    	foreach($keyword as $v=>$keyword)
    	{
    		
			static $px = 0, $zw = 0, $ezw = 0;
			$urlArray = array (); $keyArray = array (); $rankArray = array ();
			$id = $idArray[$v];
			$rsState = false;
			$enKeyword = urlencode ( $keyword );
			$firstRow = ($page - 1) * 10;
			$snoopy = new Snoopy ();
			$snoopy->fetch ( "http://www.baidu.com/s?wd=$enKeyword" );
			
			$contents = $snoopy->results;
			
			// 推广位
			$ebaidu = array (
					'lt' => 0,
					'lb' => 0,
					'r' => 0 
			); 
			
			// 记录百度推广数量，分为左上、左下、右侧
			if (preg_match_all ( "/<table id=\"30\d+\".*?class=\"ec_pp_f\".*?>|<table id=\"40\d+\".*?class=\"EC_mr15\".*?>/i", $contents, $out_lt ))
			{
				$ebaidu ['lt'] = count ( $out_lt [0] );
			}
			if (preg_match_all ( "/<table width=\"63\d+\".*?class=\"EC_mr15\">/i", $contents, $out_lb ))
			{
				$ebaidu ['lb'] = count ( $out_lb [0] );
			}
			if (preg_match_all ( "/<div id=\"bdfs\d+\" class=\"EC_fr EC_PP\".*?>/i", $contents, $out_r ))
			{
				$ebaidu ['r'] = count ( $out_r [0] );
			}
			
			$num_spread = $ebaidu ['lt'] . ',' . $ebaidu ['lb'] . ',' . $ebaidu ['r'];
			/*
			if ($page > 10)
			{
				// die('10页之内没有该网站排名..end');
				$resultArray = array (
						'snapshot' => '',
						'rank' => '',
						'compete' => $this->key_weight ( $num_spread ),
						'num_spread' => $num_spread,
						'searchdate' => date ( 'Y-m-d H:s:m' ) 
				);
				$this->saveResult ( $id, $resultArray );
				exit ();
			}
			*/
			
			preg_match_all ( '/<table[^>]*?class="result"[^>]*>[\s\S]*?<\/table>/i', $contents, $rs ); // 首页占位
			
			foreach ( $rs [0] as $k => $v )
			{
				$px ++;
				if (strstr ( $v, $unit ))
				{
					// $rsState = true;
					preg_match_all ( '/<h3[\s\S]*?(<a[\s\S]*?<\/a>)/', $v, $rs_t );
					// echo '当前 "' . $url . '" 在百度关键字 "' . $keyword . '" 中的排名为：'
					// . $px;
					// echo '第' . $page . '页;第' . ++ $k . "个<a target='_blank'
					// href='http://www.baidu.com/s?wd=$enKeyword&&pn=$firstRow'>进入百度</a>";
					// echo $rs_t[1][0];
					$k ++;
					$keyArray [] = $k;
				}
				
				preg_match_all ( '/<h3[\s\S]*?(<a[\s\S]*?<\/a>)/', $v, $rs_t );
				
				$rarray = $snoopy->_striplinks ( $rs_t [1] [0] );
				@$urlArray = array_merge ( $urlArray, $rarray );
				
			}
			

			$mh = curl_multi_init ();
			foreach ( $urlArray as $i => $url )
			{
				$conn [$i] = curl_init ( $url );
				curl_setopt ( $conn [$i], CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)" );
				curl_setopt ( $conn [$i], CURLOPT_HEADER, 0 );
				curl_setopt ( $conn [$i], CURLOPT_CONNECTTIMEOUT, 10 );
				curl_setopt ( $conn [$i], CURLOPT_RETURNTRANSFER, true ); // 设置不将爬取代码写到浏览器，而是转化为字符串
				curl_setopt ( $conn [$i], CURLOPT_FOLLOWLOCATION, true ); // 使用自动跳转
				curl_multi_add_handle ( $mh, $conn [$i] );
			}
			do
			{
				curl_multi_exec ( $mh, $active );
			} while ( $active );
			
		    unset($zw); //多次循环、莫名递增！！
		    
			foreach ( $urlArray as $i => $url )
			{
				@$zw ++;
				$data = $this->my_encoding ( curl_multi_getcontent ( $conn [$i] ), 'utf-8' ); // ("UTF-8","GBK//IGNORE",curl_multi_getcontent($conn[4]));
				if (preg_match ( "/.$unit./", $data ))
				{
					$rankArray [] = $zw;
				}
			}
			// 抓取所有链接
			/*
			 * $snoopy->fetchlinks("http://www.baidu.com/s?wd=$enKeyword");
			 * $contentlink=$snoopy->results; $a; foreach ($contentlink as
			 * $r=>$url) { if(strstr('http://www.baidu.com/link?url',$url)) {
			 * $a++; //unset($contentlink[$r]); //print_r($contentlink); } }
			 */
			
			// 写入
			//echo implode ( ',', $rankArray ).'a';
			$rank = implode ( ',', $rankArray );
			$resultArray = array (
					'snapshot' => '',
					'rank' => $rank,
					'compete' => $this->key_weight ( $num_spread ),
					'num_spread' => $num_spread,
					'searchdate' => date ( 'Y-m-d H:s:m' ) 
			);
		
			$this->saveResult ( $id, $resultArray ,'keyword_rank');
			
			//移除 
			foreach ($urlArray as $i => $url) {
				
				curl_multi_remove_handle($mh,$conn[$i]); 
				curl_close($conn[$i]); 
		    }
			
			curl_multi_close ( $mh );
			unset ( $contents, $zw, $v,$rs_t, $data, $snoopy, $id, $rarray, $urlArray, $rankArray, $resultArray, $i );
			
			// 多页查询
			/*
			 * if ($rsState === false) { $this->search($keyword, $url,$id, ++
			 * $page); }
			 */
		}
    }
    
    /**
     * 保存查询结果
     * @param $id
     * @param array $resultArray
     * @param string $db
     * @return void
     */
    public function saveResult($id,$resultArray,$db)
    {
        
        $this->db->where('id', $id);
        $this->db->update($db, $resultArray);
        //return $id;
    }
    
    /**
     * 查询搜索结果，用于前台ajax返回值
     * @param unknown_type $id
     * @param $db
     * @return array $data
     */
    public function rankResult($id,$db)
    {
		static $data;
		$this->db->select ( '*' )->from ( $db )->where ( 'id', $id );
		$q = $this->db->get ();
		if ($q->num_rows () > 0)
		{
			foreach ( $q->result_array () as $row )
			{
				$data [] = $row;
			}
		}
		return $data;
	}
	
	/**
	 * url收录查询
	 * @param $unit
	 * @param $id
	 * @return void
	 */
	public function url_search ($id,$url)
	{
		static $end_time=array(),$pageinfo = array ();
		$pageinfo ['is_record'] = 1;
		$pageinfo ['title'] = '';
		$snoopy = new Snoopy ();
		
		// 是否收录
		$snoopy->fetch ( 'http://www.baidu.com/s?wd=' . $url );
		$contents = $snoopy->results;
		if (strstr ( $contents, '抱歉，没有找到与' ))
		{
			$pageinfo ['is_record'] = 0;
		}
		// 快照日期
		$tt = "/(\d{4}\-\d{1,2}-\d{1,2})(.*?)<\/span>/i";
	    //preg_match ( $tt, $contents, $end_time );
		if (preg_match ( $tt, $contents, $end_time ))
		{
			$pageinfo ['snapshot'] = $end_time [1];
		} else
		{
			$pageinfo ['snapshot'] = '';
		}
		// 标题
		unset ( $contents );
		$snoopy->fetch ( $url );
		$contents = $snoopy->results;
		preg_match ( "/<title>(.*)<\/title>/smUi", $contents, $matches );
		$pageinfo ['title'] = trim ( $matches [1] );
		$pageinfo ['title']=$this->my_encoding ( $pageinfo ['title'], 'UTF-8' );
		
		$pageinfo['searchdate'] = date ( 'Y-m-d H:s:m' );
		
		$this->saveResult($id,$pageinfo,'keyword_url');
	}
    
}


