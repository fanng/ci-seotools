<?php

class Mexcel extends CI_Model
{
    function __construct()
    {
        // parent::Model();
        $this->load->database();
    }

    /**
     * 增加
     *
     * @param 姓名
     * @param 性别
     * @param 电话
     *
     * @return bool
     */
    function add_Excel($value)
    {
        $data = array(
            'uname' => $value['uname'],
            'usex'  => $value['usex'],
            'utel'  => $value['utel'],
        );

        return $this->db->insert('users',$data);

    }

    /**
     * 更新
     *
     * @param string $value
     *
     * @internal param $姓名
     * @internal param $性别
     * @internal param $电话
     * @internal param $ID
     * @return bool
     */
    public function upd_Excel($value = '')
    {
        $data = array(
            'uname' => $value['uname'],
            'usex'  => $value['usex'],
            'utel'  => $value['utel'],
        );
        $this->db->where('uid',$value['uid']);

        return $this->db->update('users',$data);

    }

    /**
     *查询表中的全部数据
     * @return data
     */
    public function writeExcelKeyword($where)
    {
        $this->db->select('keyword, rank,num_spread');
        $this->db->where($where);
        $query  = $this->db->get("zhanwei_keyword");
        $result = $query->result_array();
        return $result;
    }

    /**
     *查询表中的全部数据（排名数据）
     * @return data
     */
    function writeExcelPaiMing($uid = 1)
    {
        $uid = isset($_GET['uid']) ? $_GET['uid'] : '1';
        $this->db->select('title, url, is_record,snapshot');
        $this->db->where('uid',$uid);
        $query = $this->db->get("keyword_url");
        foreach ($query->result_array() as $v => $r)
        {

        }

        return $query->result_array();
    }

}

?>