<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         
 * @author           fangyang (funyung@163.com)
 * @since            2013-4-6 2:56:23
 *     
 */
 
class Baidu_rank_model extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table_name = 'keyword_url';
    }

    public function fetch_all($where = array())
	{
		$this->db->select ( '*' )->from ( $this->table_name )->where($where);
		$q = $this->db->get ();
		
		return $q->result ();
	}
    
    public function default_keyword_rank ($limit = 150, $offset = 0,$where = array())
    {
    	$data = array ();
    	//$this->db->where($where);
        $this->db->select ( '*' )
                 ->from ( $this->table_name )
                 ->where($where)
                 ->limit ( $limit, $offset )
                 ->order_by('id', 'desc');
		$q = $this->db->get ();
		
		if ($q->num_rows () > 0)
		{
			foreach ( $q->result_array () as $row )
			{
				$data [] = $row;
			}
		}
		
		return $data;
	}
    
    /**
     * 去除数组中空和重复的元素
     */
    public function delArrayRepeat($arr)
    {
        if (is_array($arr))
        {
            $arr = array_unique($arr);
            foreach ($arr as $k => $v)
            {
                if ($v == ''||empty($v))
                {
                    unset($arr[$k]);
                }
            }
            $result = $arr;
        } else
        {
            $result = false;
        }
        return $result;
    }
    
    /**
     * insert url
     * @param arrray $data
     */
    public function insert_url($data)
    {
        $this->db->insert($this->table_name,$data);
    
    }
    
    /**
     * delete keywords
     * @param string #data
     * @return $data
     */
    public function delete_keyword ($data)
    {
        foreach ($data as $v)
        {
            $this->db->delete($this->table_name, array( 'id' => $v ));
        }
        return true;
    }
    
    /**
     * get keywords
     * @return string
     */
    public function getURL($id)
	{
		static $data;
		$this->db->select ( 'url' )->from ( $this->table_name )->where ( 'id', $id );
		$q = $this->db->get ();
		if ($q->num_rows () > 0)
		{
			$row = $q->row ();
			$data = $row->url;
		}
		
		return $data;
	}
	
	/**
	 * 
	 */
	public function getHospitalunit()
	{
		static $data, $idArray = array (), $dataArray = array (), $table = 'hospital_unit';
		
		$this->db->select ( 'h_title,id' )->from ( $table )->where ( 'is_top', '1' );
		
		$q = $this->db->get ();
		
		if ($q->num_rows () > 0)
		{
			foreach ( $q->result_array () as $row )
			{
				$data [] = $row;
			}
		}
		
		return $data;
	}
	
	/**
	 * 
	 */
	public function getUnit($unit)
	{
		static $table='hospital_unit',$data;
		
		$this->db->select ( 'h_title' )->from ( $table )->where ( 'id', $unit );
		
		$q = $this->db->get ();
		
		if ($q->num_rows () > 0)
		{
			$row = $q->row ();
			$data = $row->h_title;
		}
		return $data;
	}
    
}
 