<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @title         
 * @author           fangyang (funyung@163.com)
 * @since            2013-4-6 2:56:23
 *     
 */
 
class zhanweiURL extends CI_Model
{
	private $keyword_table = 'zhanwei_url';
	/**
	 * 构造函数
	 */
    public function __construct()
    {
        parent::__construct();
        $this->load->library ( 'fx_auth' );
        $this->load->database();
        $this->table_name = 'zhanwei_url';
    }

    /**
     * 获取所有
     *
     * @param array $where
     *
     * @return array
     */
    public function fetch_all($where = array())
	{
		$this->db->select ( '*' )->from ( $this->table_name )->where($where);
		$q = $this->db->get ();
		
		return $q->result ();
	}

    /**
     *
     * @param int   $limit
     * @param int   $offset
     * @param array $where
     *
     * @return multitype:unknown
     */
    public function default_keyword_rank ($limit = 150, $offset = 0,$where = array())
    {
    	$data = array ();
    	//$this->db->where($where);
        $this->db->select ( '*' )
                 ->from ( $this->table_name )
                 ->where($where)
                 ->limit ( $limit, $offset )
                 ->order_by('id', 'desc');
		$q = $this->db->get ();
		
		if ($q->num_rows () > 0)
		{
			foreach ( $q->result_array () as $row )
			{
				$data [] = $row;
			}
		}
		
		return $data;
	}
    
    /**
     * 去除数组中空和重复的元素
     * 
     * @param unknown_type $arr
     * @return Ambigous <boolean, unknown>
     */
    public function delArrayRepeat($arr)
    {
        if (is_array($arr))
        {
            $arr = array_unique($arr);
            foreach ($arr as $k => $v)
            {
                if ($v == ''||empty($v))
                {
                    unset($arr[$k]);
                }
            }
            $result = $arr;
        } else
        {
            $result = false;
        }
        return $result;
    }
    
    /**
     * 检查是否已经存在关键词
     * 
     * @param string $keyword
     * @return boolean
     */
    function is_exist($keyword = '',$is_repeat = '')
    {
    	$is_exist= FALSE;
    	
    	if($is_repeat =='')  //允许与其他账户存在重复
    	{
    		$this ->db->where ( 'url', $keyword );
    	}else {
    		
    		$this ->db->where ( 'url', $keyword );
    		$this ->db->where ( 'author', $this->fx_auth-> get_username() );
    	}
    	
    	$q = $this->db->get ($this->keyword_table);
    	if($q->num_rows () ==0)
    	{
    		$is_exist = TRUE;
    	}
    	return $is_exist;
    }
    
    /**
     * insert keywords
     */
    public function insert_URL($data)
    {
        $this->db->insert($this->table_name,$data);
    
    }

    /**
     * delete keywords
     *
     * @param $author
     *
     * @internal param $data
     *
     * @internal param data $string
     * @return bool $data
     */
    public function deleteURL ($author)
    {
        $this->db->delete($this->table_name, array( 'author' => $author ));

        return true;
    }
    
    /**
     * get keywords
     */
    public function getKeyword($id)
	{
		unset($data);
		static $idArray=array(),$dataArray=array();
		$idArray = explode('|',$id);
		foreach($idArray as $v)
		{
			$this->db->select ( 'keyword' )->from ( $this->table_name )->where ( 'id', $v );
			$q = $this->db->get ();
			
			if ($q->num_rows () > 0)
			{
				$row = $q->row();
				$data = $row->keyword;
			}
			$dataArray[]=@$data;	
		}
		return $dataArray;
	}
	
	/**
	 * 
	 */
	public function getHospitalunit()
	{
		static $data, $idArray = array (), $dataArray = array (), $table = 'hospital_unit';
		
		$this->db->select ( 'h_title,id' )->from ( $table );
		$q = $this->db->get ();
		
		if ($q->num_rows () > 0)
		{
			foreach ( $q->result_array () as $row )
			{
				$data [] = $row;
			}
		}
		
		return $data;
	}
	
	/**
	 * 获取单元
	 * 
	 * @param  $unit
	 */
	public function getUnit($unit)
	{
		static $table='hospital_unit',$data;
		
		$this->db->select ( 'matchs' )->from ( $table )->where ( 'id', $unit );
		
		$q = $this->db->get ();
		
		if ($q->num_rows () > 0)
		{
			$row = $q->row ();
			$data = $row->matchs;
		}
		return $data;
	}
    
	/**
	 * 获取已经查询过的关键词数量、占位、推广位
	 * 
	 * @param $name
	 * @return array
	 */
	function getSearched($name = '',$uid = '')
	{
		$result = array();
		if($name != '') $this ->db->where ( 'author', $name );
		if($uid != '')  $this ->db->where ( 'uid', $uid );
	
		$this ->db->where ( 'searchdate !=', 'null' );
		$q = $this->db->get ($this->keyword_table);
	    $result['total']=$q->num_rows ();
	    
	    //占位(已经查询过的结果)
	    unset($q);
	    if($name != '') $this ->db->where ( 'author', $name );
		if($uid != '')  $this ->db->where ( 'uid', $uid );
	    $this ->db->where ( 'searchdate !=', 'null' );
	    $this ->db->where ( 'rank !=', '' );
	    $q = $this->db->get ($this->keyword_table);
	    $result['placeholder']=$q->num_rows ();
	    
	    //推广位(已经查询过的结果)
	    unset($q);
	    if($name != '') $this ->db->where ( 'author', $name );
		if($uid != '')  $this ->db->where ( 'uid', $uid );
	    $this ->db->where ( 'searchdate !=', 'null' );
	    $this ->db->where ( 'rank !=', '' );
	    $q = $this->db->get ($this->keyword_table);
	    $result['popularizeplace']=$q->num_rows ();
	    
	    return $result;
	}
	
}
 